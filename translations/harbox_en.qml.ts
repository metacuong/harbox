<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="13"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="32"/>
        <source>About HarBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="37"/>
        <source>Unofficial Box.net client for MeeGo 1.2 Harmattan
Dev by Cuong Le &lt;metacuong@gmail.com&gt;

HarBox is released under GPLv3 license</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="51"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="71"/>
        <source>Box Terms of Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="76"/>
        <source>Box and the Box logo are including without limitation, either trademarks, service marks or registered trademarks of Box, Inc., and may not be copied, imitated, or used, in whole or in part, without Box&apos;s prior written permission or that of our suppliers or licensors. Other product and company names may be trade or service marks of their respective owners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/About.qml" line="85"/>
        <source>Read more</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccInfoPage</name>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="13"/>
        <source>Account Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="68"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="69"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="70"/>
        <source>Access ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="71"/>
        <source>User ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="72"/>
        <source>Space amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="73"/>
        <source>Space used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/AccInfoPage.qml" line="74"/>
        <source>Max upload size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommentsPage</name>
    <message>
        <location filename="../harbox/qml/harbox/CommentsPage.qml" line="81"/>
        <source>No comments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileBoxPage</name>
    <message>
        <location filename="../harbox/qml/harbox/FileBoxPage.qml" line="49"/>
        <source>Choose files to upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileBoxPage.qml" line="49"/>
        <source>Upload new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileBoxPage.qml" line="190"/>
        <source>This folder is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileBoxPage.qml" line="212"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileBoxPage.qml" line="221"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileDetailPage</name>
    <message>
        <location filename="../harbox/qml/harbox/FileDetailPage.qml" line="74"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileDetailPage.qml" line="79"/>
        <source>Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileDetailPage.qml" line="97"/>
        <source>Rename complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileDetailPage.qml" line="147"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileDetailPage.qml" line="148"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileFolderActionsContent</name>
    <message>
        <location filename="../harbox/qml/harbox/FileFolderActionsContent.qml" line="26"/>
        <source>Create New Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileFolderActionsContent.qml" line="26"/>
        <source>Rename folder to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileFolderActionsContent.qml" line="26"/>
        <source>Rename file to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/FileFolderActionsContent.qml" line="42"/>
        <source>New folder here</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderMenu</name>
    <message>
        <location filename="../harbox/qml/harbox/HeaderMenu.qml" line="54"/>
        <source>My Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/HeaderMenu.qml" line="55"/>
        <source>Transfer Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/HeaderMenu.qml" line="106"/>
        <source>Log out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/HeaderMenu.qml" line="116"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogInPage</name>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="35"/>
        <source>Sign In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="41"/>
        <source>Authenticate via Box.net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="55"/>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="124"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="65"/>
        <source>Email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="85"/>
        <source>Create password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="106"/>
        <source>Confirm password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="180"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="208"/>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="221"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="209"/>
        <source>Can not get a ticket from Box.net server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="222"/>
        <source>Can not verify with Box.net server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="268"/>
        <source>Please enter a valid password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/LogInPage.qml" line="270"/>
        <source>Please enter a valid email address</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="55"/>
        <source>Pull down to update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="55"/>
        <source>Release to update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="263"/>
        <source>You are offline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="280"/>
        <source>This folder is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="301"/>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="507"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="302"/>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="508"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="323"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="438"/>
        <source>Share::Invite a collaborator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="439"/>
        <source>Share::Send a link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="440"/>
        <source>Unshare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="441"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="442"/>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="570"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="443"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="444"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="485"/>
        <source>Please select another folder and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="563"/>
        <source> folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="563"/>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="567"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="567"/>
        <source> file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="570"/>
        <source>Are you sure do you want to delete %1 ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="580"/>
        <source>Delete Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="580"/>
        <source>Delete the current folder and all of its content ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="597"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="597"/>
        <source>Are you sure do you want to delete this file ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="606"/>
        <source>No items selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="688"/>
        <source>Rename complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/MainPage.qml" line="688"/>
        <source>Folder created successfuly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MsgBox</name>
    <message>
        <location filename="../harbox/qml/harbox/MsgBox.qml" line="45"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="13"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="34"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="44"/>
        <source>API key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="65"/>
        <source>Download folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="89"/>
        <source>API Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="94"/>
        <source>Excessive use of the API either by HarBox particular API key or a user accessing Box via HarBox API key will cause HarBox app to be rate limited.

Once the limit is reached Harbox will be unable to perform any requests to Box server until the limit is reset.

You can create an API by your-self to solve this problem ;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="109"/>
        <source>Something changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="110"/>
        <source>Do you want to save your changes ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="112"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/qml/harbox/Settings.qml" line="113"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShareSendALinkContent</name>
    <message>
        <location filename="../harbox/qml/harbox/ShareSendALinkContent.qml" line="25"/>
        <source>Share - Send a link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransfersPage</name>
    <message>
        <location filename="../harbox/qml/harbox/TransfersPage.qml" line="99"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Waiting</name>
    <message>
        <location filename="../harbox/qml/harbox/Waiting.qml" line="32"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../harbox/qml/harbox/main.qml" line="21"/>
        <source>Network Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>BoxApi</name>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="434"/>
        <source>You provided an invalid api_key, or the api_key is restricted from calling this function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="436"/>
        <source>The user did not successfully authenticate on the page provided in the authentication process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="438"/>
        <source>Another error occured in your call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="440"/>
        <source>When no api_key parameter is provided</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="442"/>
        <source>Generic error for other invalid inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="444"/>
        <source>The parent folder does not exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="446"/>
        <source>A folder with the same name already exists in that location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="448"/>
        <source>The name provided for the new folder contained invalid characters or too many characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="450"/>
        <source>A folder name was not properly provided</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="452"/>
        <source>The folder name contained more than 100 characters, exceeding the folder name length limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="454"/>
        <source>A file/folder of the same name already exists in the same folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="456"/>
        <source>The file/folder name contains invalid characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="458"/>
        <source>The file/folder name exceeds the number of characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="460"/>
        <source>The user does not have the necessary permissions to perform this action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="462"/>
        <source>The target id either does not exist or is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="465"/>
        <source>Email is not a valid email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="467"/>
        <source>Email is already registered by another user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../harbox/src/boxapi.cpp" line="470"/>
        <source>An error has occurred</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../harbox/src/controller.cpp" line="405"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

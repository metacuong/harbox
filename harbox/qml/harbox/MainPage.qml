import QtQuick 1.1
import com.nokia.meego 1.1
import com.meego.extras 1.0

import "harbox_utils.js" as Utils

Page {

    id:mainPage

    orientationLock: PageOrientation.LockPortrait

    Header { title: "box"; text_bold : true; show_menu: true; onMenu_item_selected: pageStack.push(Qt.resolvedUrl(qmlPage))}

    tools: (!is_multi_select && !is_move_copy_actions)?commonTools:is_move_copy_actions?movecopyTools:multiselectTools

    /*!
      Private properties
      */

    property bool is_multi_select: false
    property bool is_move_copy_actions: false
    property bool is_current_root_folder: true
    property bool is_pull_down: true
    property bool is_just_press_back_button: false
    property string current_filename: ""

    /*!
      List
      */

    //-------------- List Header --------------

    property Component listHeaderPullDown: Component {
        Item {
            width: mainPage.width
            height: 0
            visible: !is_move_copy_actions

            Row {
                anchors.bottom: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 10

                Column {
                    Image {
                        source: "images/down.png"
                    }
                    rotation: is_pull_down?0:-180
                }

                Column {
                        Label {
                            font.pixelSize: 22
                            text: is_pull_down?qsTr("Pull down to update"):qsTr("Release to update")
                        }
                }

            }
        }
    }

    //-------------- End List Header --------------

    InfoBanner {
        id: infoBanner
        timerEnabled: true
        timerShowTime: 3000
        z:2
        parent: mainPage
    }

    //-------------- List Item Delegate --------------
    property Component listItemDelegate: Component {
        Rectangle {
            id: recItemDel
            width: mainPage.width
            height: 75
            color: model.is_locked?"#4DE8C1":(is_multi_select && model.is_selected)?"white":"transparent"

            Connections {
                target: boxListModel
                onSelectedChange: recItemDel.color = "transparent"
                onLockedChange: recItemDel.color = model.is_locked?"#4DE8C1":"transparent"
            }

            Rectangle {
                y: recItemDel.height - 1
                height: 1
                width: recItemDel.width
                color: "#C7C5C5"
                z:1
            }

            Row {
                spacing: 10
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    Rectangle {
                        visible: is_multi_select
                        x: 5
                        width: 5
                        height: 65
                        color: (is_multi_select && model.is_selected)?"blue":"grey"
                    }
                }

                Column {
                    width: 69
                    Image {
                        x:5
                        width: 64
                        height: 64
                        cache: true
                        anchors.horizontalCenter: parent.horizontalCenter
                        asynchronous: true
                        source: model.is_folder?(model.shared?"images/shared.png":"images/folder.png"):model.small_thumbnail
                    }
                }

                Column {
                    spacing: 3
                    Label {
                        text: model.name
                        color: "#395994"
                        width: recItemDel.width - 100
                        elide: Text.ElideRight
                        font.bold: model.is_folder
                        font.pixelSize: 26
                    }

                    Row {
                        spacing: 4
                        Column {
                            Text {
                                text: Controller.get_locale_date(model.updated)
                                color: "gray"
                                font.pixelSize: 20
                            }
                        }
                        Column {
                            visible: model.is_folder
                            Image {
                                source: "images/file_count.png"
                            }
                        }
                        Column {
                            Text {
                                text: model.is_folder?model.file_count:Utils.bytes_exchange(model.size)
                                font.pixelSize: 20
                            }
                        }
                    }

                }
            }

            MouseArea {
                anchors.fill: parent
                onPressAndHold: {
                    if (!is_multi_select && !is_move_copy_actions) {
                        vibration.start()
                        boxListModel.item_selected(model.index)
                        recItemDel.color = model.is_selected ? "white" : "transparent"
                        cmd_multi_select()
                    }
                }
                onClicked: {
                    if (is_multi_select) {
                        boxListModel.item_selected(model.index)
                        recItemDel.color = model.is_selected ? "white" : "transparent"
                    } else {
                        if (!model.is_locked && model.is_folder) {
                            boxListModel.folder_opened(model.index)
                            cmd_change_folder(model.id)
                        }
                        else {
                            if (!is_move_copy_actions) {
                                boxListModel.file_opened(model.id)
                                cmd_open_file(model)
                            }
                        }
                    }
                }
                onPressed : {
                    if (!model.is_locked)
                        recItemDel.color = "#E8CAD6"
                }
                onReleased: {
                    if (!model.is_locked)
                        recItemDel.color = (model.is_selected && is_multi_select)?"white":"transparent"
                }
                onCanceled: {
                    if (!model.is_locked)
                        recItemDel.color = (model.is_selected && is_multi_select)?"white":"transparent"
                }
            }                        
        }
    }
    //-------------- End List Item Delegate --------------

    //-------------- List View --------------

    ListView {
        id: boxList
        anchors.fill: parent
        anchors.topMargin: 70 + recNoconnect.height + recFolderPath.height
        model: boxListModel
        delegate: listItemDelegate
        header: listHeaderPullDown

        onContentYChanged: {
            if (!is_move_copy_actions) {
                if (is_pull_down && contentY <= -120)
                    is_pull_down = false
            }
        }

        onMovementEnded: {
            if (!is_move_copy_actions) {
                if (!is_pull_down)
                    cmd_update_current_folder()
                is_pull_down = true
            }
        }
    }

    //-------------- End List View --------------

    //-------------- Rec to lets you know what is the current folder ----------

    Rectangle {
        id:recFolderPath
        y: 70
        width: parent.width
        height: labelRec.text?40:0
        color: "white"
        property alias text: labelRec.text
        Label {
            id: labelRec
            x:10
            width: parent.width - 10
            anchors.verticalCenter: parent.verticalCenter
            elide: Text.ElideLeft
            font.pixelSize: 22
        }
    }

    //-------------- End Rec ---------------------

    //-------------- Rec to notify offline -------------

    Rectangle {
        id:recNoconnect
        visible: false
        y: recFolderPath.height + 70
        width: parent.width
        height: visible ? 40 : 0
        color: "#ccd7e4"
        Label {
            anchors.centerIn: parent
            text: qsTr("You are offline.")
            font.pixelSize: 20
        }
    }

    //-------------- End Rec to notify offline --------


    //-------------- Rec for Empty Folder -----------

    Rectangle {
        id:recEmptyFolder
        visible: false
        anchors.fill: parent
        color: "transparent"
        Label {
            anchors.centerIn: parent
            text : qsTr("This folder is empty")
            font.bold: false
            width: parent.width
            font.pixelSize: 68
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color:"#E8B7CB"
        }
    }

    //-------------- End Rec for Empty Folder ------

    //-------------- Sheet for rename/createnew a folder or rename a file -----------

    FileFolderActionsContent {
         id:ffActionsContent
         onTextchanged: file_folder_rename_or_createnewSheet.acceptButton.enabled = ffActionsContent.textVal.length
        }
    Sheet {
        id: file_folder_rename_or_createnewSheet

        acceptButtonText: qsTr("OK")
        rejectButtonText: qsTr("Cancel")

        property Button acceptButton

        content: ffActionsContent

        onAccepted: cmd_file_folder_actions_emit()

        Component.onCompleted: {
            file_folder_rename_or_createnewSheet.acceptButton = file_folder_rename_or_createnewSheet.getButton("acceptButton")
            file_folder_rename_or_createnewSheet.acceptButton.platformStyle = btplatformStyle
            file_folder_rename_or_createnewSheet.acceptButton.enabled= false
        }
    }

    //-------------- End Sheet -----------

    //-------------- Sheet for Share::Send a link ---------------
    ShareSendALinkContent{id: ssalContent}
    Sheet {
        id: share_send_a_link
        rejectButtonText: qsTr("Done")
        content: ssalContent
    }

    //-------------- End Sheet for Share::Send a link ----------

    /*!
      End List
      */

    /*!
      Toolbars
      */

    // common toolbar
    ToolBarLayout {
        id: commonTools
        visible: !is_multi_select

        ToolIcon {
            platformIconId: "toolbar-back"
            visible: !is_current_root_folder
            onClicked: cmd_back_to_parent_folder()
        }

        ToolIcon {
            platformIconId: "toolbar-add"
            onClicked: cmd_create_new_folder()
        }

        ToolIcon {
            platformIconId: "toolbar-up"
            onClicked: cmd_choose_files_to_upload()
        }

        ToolIcon {
            platformIconId: "toolbar-refresh"
            onClicked: cmd_update_current_folder()
        }

        ToolIcon {
            platformIconId: "toolbar-list"
            onClicked: cmd_multi_select()
        }

        ToolIcon {
            id:toolContextMenu
            visible: !is_current_root_folder
            platformIconId: "toolbar-view-menu"
            onClicked:  (contextMenu.status === DialogStatus.Closed) ? contextMenu.open() : contextMenu.close()
        }
    }

    //multiselect toolbar
    ToolBarLayout {
        id: multiselectTools
        visible: is_multi_select

        ToolIcon {
            platformIconId: "icon-m-invitation-declined"
            onClicked: cmd_multi_select_deselected()
        }

        ToolIcon { //move
            platformIconId: "icon-m-toolbar-directory-move-to"
            onClicked: cmd_lock_items_selected_to_move_or_copy(false)
        }

        ToolIcon { //copy
            platformIconId: "icon-m-toolbar-jump-to-dimmed-white"
            onClicked: cmd_lock_items_selected_to_move_or_copy(true)
        }

        ToolIcon {
            platformIconId: "icon-m-toolbar-delete"
            onClicked: cmd_delete_items_selected()
        }

    }

    //move/copy toolbar
    ToolBarLayout {
        id: movecopyTools
        visible: is_move_copy_actions

        ToolIcon {
            platformIconId: "toolbar-back"
            visible: !is_current_root_folder
            onClicked: cmd_back_to_parent_folder()
        }

        ToolIcon {
            platformIconId: "icon-m-invitation-declined"
            onClicked: cmd_move_copy_actions_cancel()
        }

        ToolIcon {
            platformIconId: "icon-m-invitation-accept"
            onClicked: cmd_move_copy_actions_accept()
        }

        ToolIcon {
            platformIconId: "toolbar-add"
            onClicked: cmd_create_new_folder()
        }

    }


    /*! Menu bar */

    ContextMenu {
        id: contextMenu

        MenuLayout {            
            MenuItem {text: qsTr("Share::Invite a collaborator")}
            MenuItem {text: qsTr("Share::Send a link"); onClicked: cmd_share_send_a_link(1)}
            MenuItem {text: qsTr("Unshare"); onClicked: cmd_unshare()}
            MenuItem {text: qsTr("Rename"); onClicked: cmd_rename_folder()}
            MenuItem {text: qsTr("Delete"); onClicked: cmd_delete_current_folder()}
            MenuItem {text: qsTr("Copy"); onClicked: cmd_lock_current_item_openned(true)}
            MenuItem {text: qsTr("Move"); onClicked: cmd_lock_current_item_openned(false)}
        }
    }

    /*!
      End Toolbars
      */

    Component.onCompleted: {
        appWindow.showToolBar = true
        theme.inverted = false
        cmd_change_folder(0)        
    }

    /*!
      Connection to Controller
      */

    Connections {
        target: Controller

        onLogout_ok: success_logout()

        onGet_account_tree_ok: {            
            success_status()
            check_folder_is_empty()
            restore_listview_contentY()                        
        }
        onCreate_folder_ok: success_file_folder_actions(0)
        onRename_folder_ok: success_file_folder_actions(1)

        onError_on_get_account_tree: error_status()        
        onError_on_create_folder: error_file_folder_actions()                
        onError_on_rename_folder: error_file_folder_actions()

        onMulti_items_delete_done: {}

        onNetwork_ok: recNoconnect.visible = false
        onError_on_network: recNoconnect.visible = true

        onSelect_another_folder: {
            infoBanner.text = qsTr("Please select another folder and try again.")
            infoBanner.show()
        }

        onMove_or_copy_finished: cmd_move_copy_actions_cancel()

        onStart_Request: {
            ma_waiting.open()
        }
        onFinished_Request: {
            ma_waiting.close()
        }

    }

    /*!
      Query dialog
      */

    QueryDialog {
        id: queryDialog

        acceptButtonText: qsTr("OK")
        rejectButtonText: qsTr("Cancel")

        onAccepted: Utils.queryDialog_accept()

        function appear(__title, __message, __fun) {
            titleText = __title
            message = __message
            Utils.queryDialog_accept = __fun
            open()
        }
    }

    /*!
      Internal functions
      */

    function cmd_multi_select() {
        is_multi_select  = true
        mainPage.pageStack.toolBar.setTools(multiselectTools, "replace")

    }

    function cmd_multi_select_deselected()  {
        is_multi_select  = false
        boxListModel.clearSelection()
        boxListModel.clearItemsSelected()
        mainPage.pageStack.toolBar.setTools(commonTools, "replace")
    }

    function cmd_lock_items_selected_to_move_or_copy(ctype) {
        if (cmd_check_items_selected()) {
            is_multi_select  = false
            is_move_copy_actions = true
            Controller.lock_current_folder(ctype)
            boxListModel.lock_items_selected()
            mainPage.pageStack.toolBar.setTools(movecopyTools, "replace")
        }
    }

    function cmd_move_copy_actions_cancel() {
        is_move_copy_actions = false
        boxListModel.clearItemsSelected()
        Controller.unlock_last_folder()
        mainPage.pageStack.toolBar.setTools(commonTools, "replace")
    }

    function cmd_move_copy_actions_accept() {
        Controller.move_or_copy_to()
    }

    function cmd_delete_items_selected() {
        if (cmd_check_items_selected()) {
            var ffSelectedCount = boxListModel.how_many_folders_files_selected()
            var ask = ""
            if (ffSelectedCount[0])
                ask = ffSelectedCount[0] + qsTr(" folder") + (ffSelectedCount[0]>1?qsTr("s"):"")
            if (ffSelectedCount[1]) {
                if (ask)
                    ask+= " | "
                ask += ffSelectedCount[1] + qsTr(" file") + (ffSelectedCount[1]>1?qsTr("s"):"")
            }

            queryDialog.appear(qsTr("Delete"), Utils.args(qsTr("Are you sure do you want to delete %1 ?"), [ask]), function() {
                    is_move_copy_actions = false
                    is_multi_select = false
                    mainPage.pageStack.toolBar.setTools(commonTools, "replace")
                    Controller.delete_items_selected()
                    })
        }
    }

    function cmd_delete_current_folder() {
        queryDialog.appear(qsTr("Delete Folder"), qsTr("Delete the current folder and all of its content ?"), function() {
                               Controller.delete_current_folder()
                           })
    }

    function cmd_lock_current_item_openned(needBack, cType) {
        if (needBack)
            cmd_back_to_parent_folder()

        is_multi_select  = false
        is_move_copy_actions = true
        Controller.lock_current_folder(cType)
        boxListModel.force_item_selected()
        mainPage.pageStack.toolBar.setTools(movecopyTools, "replace")
    }

    function cmd_delete_file() {
        queryDialog.appear(qsTr("Delete File"), qsTr("Are you sure do you want to delete this file ?"), function() {
                               Controller.delete_current_file()
                           })
    }

    function cmd_check_items_selected() {
        if (boxListModel.has_selected())
            return true
        else {
            infoBanner.text = qsTr("No items selected")
            infoBanner.show()
            return false
        }
    }

    function cmd_change_folder(folder_id) {
        is_current_root_folder = folder_id?false:true
        is_just_press_back_button = false
        backup_listview_contentY()
        Controller.get_account_tree(folder_id)
    }

    function cmd_back_to_parent_folder() {
        is_current_root_folder = Controller.back_parent_folder_is_root()
        is_just_press_back_button = true
        Controller.back_to_parent_folder()
    }

    function cmd_update_current_folder() {
        recEmptyFolder.visible = false
        is_just_press_back_button = false
        Controller.update_current_folder()
    }

    function cmd_open_file(model) {
        current_filename = model.name
        ffActionsContent.object_id = model.id
        Controller.change_object_id(model.id)
        pageStack.push(Qt.resolvedUrl("FileDetailPage.qml"),
                       {obj: mainPage, filename:model.name, updated: Controller.get_locale_date(model.updated),
                        size: Utils.bytes_exchange(model.size), url:model.preview_thumbnail}
                       )
    }

    function cmd_create_new_folder() {
        ffActionsContent.state = 0
        ffActionsContent.textVal = ""
        file_folder_rename_or_createnewSheet.open()
    }

    function cmd_rename_folder() {
        ffActionsContent.state = 1
        ffActionsContent.textVal = Controller.get_current_folder_name()
        file_folder_rename_or_createnewSheet.open()
    }

    function cmd_rename_file() {
        ffActionsContent.state = 2
        ffActionsContent.textVal = current_filename
        file_folder_rename_or_createnewSheet.open()
    }

    function cmd_file_folder_actions_emit() {
        Controller.file_folder_actions_emit(ffActionsContent.state, ffActionsContent.textVal, ffActionsContent.object_id)
    }

    function error_status() {
        infoBanner.text = Controller.get_current_error_string()
        if (infoBanner.text)
            infoBanner.show()
        reload_full_Path()
        is_current_root_folder = Controller.current_folder_is_root()
    }

    function success_status() {        
        reload_full_Path()
        is_current_root_folder = Controller.current_folder_is_root()
    }

    function network_error() {
        is_current_root_folder = Controller.current_folder_is_root()
    }

    function success_logout() {
        pageStack.pop()
        pageStack.push(Qt.resolvedUrl("LogInPage.qml"))
    }

    function success_file_folder_actions(actionType) {
        if (actionType)
            reload_full_Path()
        infoBanner.text = actionType?qsTr("Rename complete"):qsTr("Folder created successfuly")
        infoBanner.show()
    }

    function error_file_folder_actions() {
        infoBanner.text = Controller.get_current_error_string()
        infoBanner.show()
    }

    function check_folder_is_empty() {
        recEmptyFolder.visible = Controller.check_current_folder_is_empty()
    }

    function restore_listview_contentY() {
        if (is_just_press_back_button)
            boxList.contentY = Utils.contentY_pop()
    }

    function backup_listview_contentY() {
        Utils.contentY_push(boxList.contentY)
    }

    function cmd_choose_files_to_upload() {
        pageStack.push(Qt.resolvedUrl("FileBoxPage.qml"), {fromDirect:true})
    }

    function cmd_share_send_a_link(target) {
        ssalContent.selectAll()
        ssalContent.textVal = ""
        Controller.create_share_link(target)
        share_send_a_link.open()
    }

    function cmd_unshare() {
        Controller.unshare(1)
    }

    function reload_full_Path() {
        recFolderPath.text = Controller.get_full_path()
    }

}

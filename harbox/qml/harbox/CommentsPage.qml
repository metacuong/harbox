import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: commentsPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: "Comments"; text_bold : true; show_menu: false}

    tools: commonTools


    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
    }

    property Component commentItemDelegate: Component {
        Column {
            spacing: 5
            Column {
                spacing: 5
                x:5
                width: commentsPage.width - 10
                Label {
                    text: model.user_name
                    font.pixelSize: 18
                    font.bold: true
                    color: "#395994"
                }
                Text {
                    width: parent.width
                    text: model.message
                    font.pixelSize: 22
                    wrapMode: Text.WordWrap
                }
                Label {
                    text: model.created
                    width: parent.width
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: 18
                    color:"gray"
                }
                Rectangle {
                    width: parent.width
                    color: "gray"
                    height: 1
                }
            }
        }
    }

    ListModel {
        id: commentsModel
    }

    ListView {
        id: commentsView
        anchors.fill: parent
        anchors.topMargin: 70
        model: commentsModel
        delegate: commentItemDelegate

    }

    Rectangle {
        id:recEmptyLog
        visible: false
        anchors.fill: parent
        color: "transparent"
        Label {
            anchors.centerIn: parent
            text : qsTr("No comments")
            font.bold: false
            width: parent.width
            font.pixelSize: 68
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color:"#E8B7CB"
        }
    }

    Connections {
        target: Controller
        onGet_comments_ok: {
            var i=0
            while(i<comments.length) {
                commentsModel.append({user_name:comments[i], message:comments[i+1], created:Controller.get_locale_date(comments[i+2])})
                i+=3
            }
            recEmptyLog.visible = i === 0
        }
    }

    Component.onCompleted: Controller.getComments()
}

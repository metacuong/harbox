import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: updatesPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: "Updates"; text_bold : true; show_menu: false}

    tools: commonTools


    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
        ToolIcon {
            platformIconId: "toolbar-refresh"
            onClicked: getUpdates()
        }
    }

    property Component updateItemDelegate: Component {
        Rectangle {
            id: recItemDel
            width: updatesPage.width
            height: 100
            color: "transparent"

            Rectangle {
                y: recItemDel.height - 1
                height: 1
                width: recItemDel.width
                color: "grey"
                z:1
            }

            Row {
                spacing: 10
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    width: 69
                    Image {
                        x:5
                        width: 64
                        height: 64
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: model.is_folder?"images/folder.png":model.t_url
                    }
                }

                Column {
                    spacing: 3
                    Label {
                        text: model.name
                        width: recItemDel.width - 80
                        elide: Text.ElideRight
                        color: "#395994"
                        font.pixelSize: 28
                    }

                    Row {
                        spacing: 4
                        Column {
                            Text {
                                text: model.updated
                                color: "gray"
                                font.pixelSize: 20
                            }
                        }
                        Column {
                            visible: model.is_folder
                            Image {
                                source: "images/file_count.png"
                            }
                        }
                        Column {
                            Text {
                                text: model.is_folder?model.file_count:model.file_size
                                font.pixelSize: 20
                            }
                        }
                    }

                    Row {
                        spacing: 4
                        Column {
                            Text {
                                text: model.updated_type
                                color: "grey"
                                font.pixelSize: 18
                            }
                        }
                        Column {
                            Text {
                                text: model.update_by
                                color: "black"
                                font.pixelSize: 18
                            }
                        }
                    }

                }

            }
        }
    }

    ListView {
        anchors.fill: parent
        anchors.topMargin: 70
        model: updatesModel
        delegate: updateItemDelegate
    }

    ListModel {
        id:updatesModel
    }

    Component.onCompleted: getUpdates()

    function getUpdates() {
        //Controller.getUpdates();

        updatesModel.clear()
        updatesModel.append({name:"Test Folder", updated_type: "added by", update_by: "metacuong@gmail.com", file_size:"", updated: "21/11/2012", file_count:0, size: 0, is_folder:true, t_url:""})
    }

}

import QtQuick 1.1
import QtWebKit 1.0
import com.nokia.meego 1.1
import com.meego.extras 1.0

Item {
    id: privacy_policy_content

    anchors.fill: parent

    property alias url: webView.url

    Flickable {
        id: flickablewebView
        width: parent.width
        height:  parent.height
        contentWidth: parent.width
        contentHeight: Math.max(parent.height, 4250)
        interactive: true
        clip: true
        pressDelay: 200

        BusyIndicator {
            id: webBusy
            visible:false
            anchors.horizontalCenter: parent.horizontalCenter
            y: 300
            z:1
            running: true
            platformStyle: BusyIndicatorStyle { size: "large";  period: 400; spinnerFrames:"image://theme/spinner"}
        }

        WebView {
         id: webView
         anchors.fill: parent
         preferredWidth: flickablewebView.width
         preferredHeight: flickablewebView.height
         focus: true
         smooth: false
         onLoadStarted: webBusy.visible=true
         onLoadFailed: webBusy.visible=false
         onLoadFinished: {
             webBusy.visible=false
         }
        }
     }
}

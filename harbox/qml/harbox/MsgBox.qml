import QtQuick 1.1
import com.nokia.meego 1.1

Rectangle {
    id: msgBox

    property alias caption: t_caption.text
    property alias title: t_title.text

    color: "black"
    opacity: 0.9
    anchors.fill: parent

    MouseArea {
        anchors.fill: parent
    }

    Rectangle {
        color: "#ecd5d5"
        width: 400
        height: 200
        anchors.centerIn: parent

        Text {
            id: t_caption
            y: 15
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 26
        }

        Text {
            id: t_title
            y: 55
            width: 369
            height: 68
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            font.pixelSize: 24
            color: "blue"
        }

        Button {
            y: 140
            text : qsTr("Ok")
            width: 140
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 22
            onClicked: msgBox.visible = false
        }

    }

}

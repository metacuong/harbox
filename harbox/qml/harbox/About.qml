import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: aboutPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: qsTr("About"); text_bold : true; show_menu: false}

    Flickable {
        id: fickableMain
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: loginZone.height + loginZone.y + 10

            Column {
                id: loginZone
                y: 80
                spacing: 20

                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("About HarBox")
                            x:10
                        }

                        Label {
                            text: qsTr("Unofficial Box.net client for MeeGo 1.2 Harmattan\nDev by Cuong Le <metacuong@gmail.com>\n\nHarBox is released under GPLv3 license")
                            color: "#395994"
                            x: 30
                            font.pixelSize: 20
                        }
                    }
                }

                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("Version")
                            x:10
                        }

                        Label {
                            text: Controller.applicationVersion()
                            color: "#395994"
                            x: 30
                            font.pixelSize: 20
                        }
                    }
                }


                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("Box Terms of Service")
                            x:10
                        }

                        Label {
                            text: qsTr("Box and the Box logo are including without limitation, either trademarks, service marks or registered trademarks of Box, Inc., and may not be copied, imitated, or used, in whole or in part, without Box's prior written permission or that of our suppliers or licensors. Other product and company names may be trade or service marks of their respective owners.")
                            width: parent.width - 50
                            wrapMode: Text.WordWrap
                            color: "#395994"
                            x: 30
                            font.pixelSize: 20
                        }

                        Button {
                            text: qsTr("Read more")
                            font.pixelSize: 22
                            anchors.horizontalCenter: parent.horizontalCenter
                            onClicked: Qt.openUrlExternally("https://www.box.com/static/html/terms.html")
                        }
                    }
            }
        }
    }

    tools: commonTools

    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
    }

}

import QtQuick 1.1
import com.nokia.meego 1.1

MouseArea {
    id:waitingArea

    visible: false

    property alias showCancel: btn_cancel.visible

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.3
    }

    Column {
        width:160
        anchors.centerIn: parent
        spacing: 20

        BusyIndicator {
            anchors.horizontalCenter: parent.horizontalCenter
            z:1
            running: true
            platformStyle: BusyIndicatorStyle { size: "large";  period: 400; spinnerFrames:"image://theme/spinnerinverted"}
        }

        Button {
            id: btn_cancel
            visible: false
            text:  qsTr("Cancel")
            width: 160
            onClicked: {close(); pageStack.pop()}
            font.pixelSize: 22
        }
    }

    function open() {
        visible = true;
        btn_cancel.visible = false;
    }

    function close() {
        visible = false;
        btn_cancel.visible = false;
    }

}

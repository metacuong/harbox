import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: accinfoPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: qsTr("Account Info"); text_bold : true; show_menu: false}

    Flickable {
        anchors.fill: parent
        anchors.topMargin: header.height + 10
        contentWidth: parent.width
        contentHeight: 300

        Grid {
            id: gridMenu
            spacing: 3
            columns: 1
            anchors.fill: parent
            anchors.leftMargin: 20
            anchors.topMargin: 5

            Repeater {
                id: qRepeater
                Row {
                    width: gridMenu.width
                    height: 40
                    Column {
                        width: 160
                        Label {
                            font.pixelSize: 22
                            text : modelData.title
                        }
                    }
                    Column {
                        Label {
                            font.pixelSize: 22
                            color: "#395994"
                            text : modelData.value
                        }
                    }
                }
            }
        }

    }    

    tools: commonTools

    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
    }

    Connections {
        target: Controller        
        onGet_acc_info_ok: {
            qRepeater.model = [
                        {title:qsTr("Login"), value: accInfo[0]},
                        {title:qsTr("Email"), value: accInfo[1]},
                        {title:qsTr("Access ID"), value: accInfo[2]},
                        {title:qsTr("User ID"), value: accInfo[3]},
                        {title:qsTr("Space amount"), value: Utils.bytes_exchange(accInfo[4])},
                        {title:qsTr("Space used"), value: Utils.bytes_exchange(accInfo[5])},
                        {title:qsTr("Max upload size"), value: Utils.bytes_exchange(accInfo[6])}]
        }
    }

    Component.onCompleted: Controller.get_account_info()

}

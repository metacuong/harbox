import QtQuick 1.1
import com.nokia.meego 1.1

Item {
    id: shareSendALinkContent

    anchors.fill: parent

    property int state: 0
    property alias textVal: tf.text

    signal textchanged()

    Rectangle {
        id: top_banner
        x: -1
        y: -1
        width: parent.width + 1
        height: 60
        color: "#395994"

        Label {
            id:h_title
            anchors.centerIn: parent
            text: qsTr("Share - Send a link")
            color: "#ffffff"
            font.pixelSize: 26
            font.bold: true
        }
    }

    Rectangle {
        y: top_banner.height
        width: parent.width
        height: 160
        color:"white"
        TextArea {
            id: tf
            anchors.fill: parent
            font.pixelSize: 22
            platformStyle: TextFieldStyle {
                background:""
                backgroundSelected: ""
                backgroundDisabled: ""
            }
            readOnly: true
        }
    }

    Connections {
        target: Controller
        onCreated_share_link: tf.text = share_link
    }

    function selectAll() {
        tf.selectAll()
    }
}

var listViewContentY = new Array();
var listViewCurrentFolder = 0;

var queryDialog_accept;

function is_not_empty(val) {
    var v_temp = trim11(val);
    if (v_temp)
        return true;
    else
        return false;
}

function is_folder_name_allowed(val){
    var ip = /[/:?*<>\\"|]/g;
    return !ip.test(val) && (val!==".") && (val!=="..");
}

function is_email(email) {
    var email_pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return email_pattern.test(email);
}

function trim11(str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
                if (/\S/.test(str.charAt(i))) {
                        str = str.substring(0, i + 1);
                        break;
                }
        }
        return str;
}

function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function ltrim(stringToTrim) {
        return stringToTrim.replace(/^\s+/,"");
}

function rtrim(stringToTrim) {
        return stringToTrim.replace(/\s+$/,"");
}

function contentY_push( val) {
    listViewContentY [listViewCurrentFolder++] = val;
}

function contentY_pop() {
    return listViewContentY [--listViewCurrentFolder];
}

function bytes_exchange(bytes) {
    var s = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
    if (bytes) {
        var e = Math.floor(Math.log(bytes)/Math.log(1024));
        return (bytes/Math.pow(1024, Math.floor(e))).toFixed(2)+" "+s[e];
    }

    return bytes + " " + s[0];
}

function args(str, params) {
    var __str;
    for(var i=0;i<params.length;i++)
        __str = str.replace("%"+(i+1), params[i]);
    return __str;
}

import QtQuick 1.1
import com.nokia.meego 1.0

Item {

    y:70
    visible: false

    signal menu_selected(string qmlPage)

    Rectangle {
        anchors.fill: parent
        color: "grey"
        opacity: 0.9

        MouseArea {
            anchors.fill: parent
            onClicked: close()
        }
    }

    Rectangle {
        y : -840
        id: menuSlide
        width : parent.width
        height : 260
        color : "#395994"
        z:2


        MouseArea {
            anchors.fill: parent
        }

        Rectangle {
            id: contextMenu
            width : parent.width - 40
            x :  20
            y : 20
            height : 140
            color : "white"
            radius: 12

            Grid {
                id: gridMenu
                spacing: 3
                columns: 3
                anchors.fill: parent
                anchors.leftMargin: 20
                anchors.topMargin: 5

                Repeater {
                    model: [
                        {title:qsTr("My Account"), image:"images/myaccount.png", qmlpage:"AccInfoPage.qml"},
                        {title:qsTr("Transfer Logs"), image:"images/filetransfer.png", qmlpage:"TransfersPage.qml"}
                        //{title:qsTr("Updates"), image:"images/updates.png", qmlpage:"UpdatesPage.qml"}
                    ]
                    Rectangle { color: "white"; width: 128; height: 128
                    Image {
                        id:menuImage
                        anchors.horizontalCenter: parent.horizontalCenter
                        y:15
                        source: modelData.image
                    }
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        y:menuImage.height + 25
                        font.pixelSize: 20
                        text : modelData.title
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {menu_selected(modelData.qmlpage);close()}
                        onPressed: parent.color = "#F0E6EA"
                        onCanceled: parent.color = "white"
                        onReleased: parent.color = "white"
                    }
                }
                }
            }

        }

        Image {
            x: 410
            y: 191
            smooth: false
            clip: false
            source: "images/settings.png"

            MouseArea {
                id: mouse_area1
                anchors.fill:parent
                clip: false
                onClicked: cmd_settings()
            }
        }

        Button {
            y: 185
            x: 20
            width:160
            font.pixelSize: 22
            font.bold: false
            text: qsTr("Log out")
            onClicked: cmd_logout()
        }

        Button {
            y: 185
            x: 200
            width:160
            font.pixelSize: 22
            font.bold: false
            text: qsTr("About")
            onClicked: cmd_about()
        }
    }

    states: [
        State {
            name: "show"
            AnchorChanges { target: menuSlide; anchors.top: parent.top}
        },
        State {
            name: "close"
            AnchorChanges { target: menuSlide; anchors.bottom: parent.top}
        }
    ]

    transitions: Transition {
        AnchorAnimation { easing.type: Easing.OutQuart; duration: 500 }
       }

    function open(){
        appWindow.showToolBar = false
        visible = true
        state = "show"
    }

    function close(){
        appWindow.showToolBar = true
        visible = false
        state = "close"
    }

    function cmd_logout() {
        close()
        Controller.logout()
    }

    function cmd_settings() {
        close()
        pageStack.push(Qt.resolvedUrl("Settings.qml"))
    }

    function cmd_about() {
        close()
        pageStack.push(Qt.resolvedUrl("About.qml"))
    }
}

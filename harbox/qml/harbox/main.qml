import QtQuick 1.1
import com.nokia.meego 1.1
import com.meego.extras 1.0
import QtMobility.feedback 1.1

PageStackWindow {
    id: appWindow

    showToolBar : false

    property QtObject btplatformStyle: SheetButtonAccentStyle {
        disabledTextColor: "gray"
        background: "image://theme/color3-meegotouch-sheet-button-accent-background"
        pressedBackground: "image://theme/color3-meegotouch-sheet-button-accent-background-pressed"
        disabledBackground: "image://theme/color3-meegotouch-sheet-button-accent-background-disabled"
    }

    Connections {
        target: Controller
        onError_on_network: {
            msgBox.caption = qsTr("Network Error")
            msgBox.title = error_string
            msgBox.visible = true
        }
    }

    MsgBox {
        id:msgBox
        visible: false
    }

    Waiting {id:ma_waiting; anchors.fill: parent}

    HapticsEffect {
        id: vibration
        attackIntensity: 0.0
        attackTime: 250
        intensity: 1.0
        duration: 250
        fadeTime: 250
        fadeIntensity: 0.0
    }

    Component.onCompleted: {
         theme.inverted = true
         pageStack.push(Qt.resolvedUrl(Controller.client_is_authenticated()?"MainPage.qml":"LogInPage.qml"))
    }

}

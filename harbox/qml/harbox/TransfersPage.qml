import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: transferPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: "Transfers Log"; text_bold : true; show_menu: false}

    tools: commonTools


    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
        ToolIcon {
            platformIconId: "toolbar-refresh"
            onClicked: getLogs()
        }
        ToolIcon {
            platformIconId: "icon-m-toolbar-delete"
            onClicked: clearLogs()
        }
    }

    property Component logItemDelegate: Component {
        Column {
            spacing: 4
            width: transferPage.width
            Label {
                text: model.name
                elide: Text.ElideMiddle
                width: parent.width - 10
                font.pixelSize: 24
                x:10
            }
            Row {
                spacing: 10
                x:10
                Label {
                    text: model.datetime
                    font.pixelSize: 18
                }
                Label {
                    text: model.size
                    font.pixelSize: 18
                    font.bold: true
                }
            }
            Row {
                spacing: 10
                x:10
                Label {
                    text: model.type?"Download":"Upload"
                    font.pixelSize: 18
                }
                Label {
                    text: model.result===1?"Failed":(model.result===2?"Cancelled":"Completed")
                    font.pixelSize: 18
                    font.bold: true
                }
            }
            Rectangle {
                y: parent.height - 1
                height: 1
                width: parent.width
                color: "#C7C5C5"
                z:1
            }
        }
    }

    ListView {
        anchors.fill: parent
        anchors.topMargin: 70
        model: logsModel
        delegate: logItemDelegate
    }

    ListModel {
        id:logsModel
    }

    Rectangle {
        id:recEmptyLog
        visible: true
        anchors.fill: parent
        color: "transparent"
        Label {
            anchors.centerIn: parent
            text : qsTr("No logs")
            font.bold: false
            width: parent.width
            font.pixelSize: 68
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color:"#E8B7CB"
        }
    }

    Component.onCompleted: getLogs()

    function getLogs() {
        /*name, size, type, result, datetime, note*/
        logsModel.clear()
        var allLogs = Controller.getAllLogs()
        var i=0
        while(i<allLogs.length) {
            logsModel.append({name:allLogs[i], size:Utils.bytes_exchange(allLogs[i+1]), type:allLogs[i+2], result: allLogs[i+3], datetime: Controller.get_locale_date(allLogs[i+4]) })
            i+=6
        }
        recEmptyLog.visible = i === 0
    }

    function clearLogs() {
        logsModel.clear()
        Controller.clearLogs()
        recEmptyLog.visible = true
    }
}

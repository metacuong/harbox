import QtQuick 1.1
import QtWebKit 1.0
import com.nokia.meego 1.1
import com.meego.extras 1.0

import "harbox_utils.js" as Utils

Page {

    id: loginPage

    orientationLock: PageOrientation.LockPortrait

    Header { title: "box"; text_bold : true; show_menu: false}

    property bool m_cancelled: false
    property bool isRegistering: false

    Flickable {
        id: fickableMain
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: loginZone.height + loginZone.y + 10

            Column {
                id: loginZone
                y: 80
                spacing: 20

                Row {
                    Column {
                        width:fickableMain.width
                        spacing: 10

                        Label {
                            text: qsTr("Sign In")
                            x:10
                        }

                        Button {
                            id: bt_auth_via_box
                            text: qsTr("Authenticate via Box.net")
                            font.pixelSize: 22
                            anchors.horizontalCenter: parent.horizontalCenter
                            onClicked: cmd_authenticate_via_box_net()
                        }
                    }
                } // end Log In

                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("Register")
                            x:10
                        }

                        Row {
                            spacing: 5
                            Column {
                                width:420
                                spacing: 5
                                Label {
                                    text: qsTr("Email address")
                                    font.pixelSize: 22
                                    x:30
                                }
                                TextField {
                                    id: tfEmail
                                    x:30
                                    width: parent.width
                                    font.pixelSize: 22
                                    onTextChanged: regTextChanged()
                                }
                            }
                        }

                        Row {
                            spacing: 5
                            Column {
                                width:420
                                spacing: 5
                                Label {
                                    text: qsTr("Create password")
                                    x:30
                                    font.pixelSize: 22
                                }
                                TextField {
                                    id: tfPassword
                                    x:30
                                    width: parent.width
                                    font.pixelSize: 22
                                    echoMode: TextInput.Password
                                    onTextChanged: regTextChanged()
                                }
                            }
                        }

                        Row {
                            spacing: 5
                            Column {
                                width:420
                                spacing: 5
                                Label {
                                    text: qsTr("Confirm password")
                                    x:30
                                    font.pixelSize: 22
                                }
                                TextField {
                                    id:tfConfirmPassword
                                    x:30
                                    width: parent.width
                                    font.pixelSize: 22
                                    echoMode: TextInput.Password
                                    onTextChanged: regTextChanged()
                                }
                            }
                        }

                        Button {
                            id: btRegister
                            enabled: false
                            text: qsTr("Register")
                            font.pixelSize: 22
                            anchors.horizontalCenter: parent.horizontalCenter
                            onClicked: cmd_register()
                        }
                    }
            } // end Register

                Row {
                    Column {
                        width:fickableMain.width
                        spacing: 20

                        Rectangle {
                            color: "white"
                            border.color: "gray"
                            height: 1
                            width: parent.width
                        }

                        Button {
                            id: bt_open_privacy_policy
                            text: qsTr("Privacy Policy (EN)")
                            font.pixelSize: 22
                            anchors.horizontalCenter: parent.horizontalCenter
                            onClicked: cmd_open_privacy_policy()
                        }
                    }
                } // end Privacy Policy
        }
    }

    MsgBox {
        id:msgBox
        visible: false
    } // end MsgBox

    Flickable {
        id: flickablewebView
        width: parent.width
        height:  parent.height
        contentWidth: parent.width
        contentHeight: Math.max(parent.height, 900)
        interactive: true
        clip: true
        pressDelay: 200

        BusyIndicator {
            id: webBusy
            visible:false
            anchors.horizontalCenter: parent.horizontalCenter
            y: 300
            z:1
            running: true
            platformStyle: BusyIndicatorStyle { size: "large";  period: 400; spinnerFrames:"image://theme/spinner"}
        }

        WebView {
         id: webView
         anchors.fill: parent
         preferredWidth: flickablewebView.width
         preferredHeight: flickablewebView.height
         focus: true
         smooth: false
         onLoadStarted: {m_cancelled = false; webBusy.visible=true}
         onLoadFailed: webBusy.visible=false
         onLoadFinished: {
             webBusy.visible=false
             if (!m_cancelled && (webView.html.indexOf("authorized")>0 || webView.html.indexOf("<strong>HarBox</strong>")>0)) {
                 sheet_for_auth_via_box_net.close()
                 cmd_verify()
             }
         }
        }
     }

    Sheet {
        id: sheet_for_auth_via_box_net
        rejectButtonText: qsTr("Cancel")
        content: flickablewebView
        onRejected: {
            webBusy.visible=false
            m_cancelled = true
            webView.url=""
        }
    }

    Sheet {
        id: sheet_for_privacy_policy
        property alias url:privacyPolicyContent.url
        rejectButtonText: qsTr("Reject")
        acceptButtonText: qsTr("Accept")
        content: PrivacyPolicyContent{id:privacyPolicyContent}
        onRejected: Qt.quit()
        onAccepted: cmd_continue_doing_something()
    }

    InfoBanner {
        id: infoBanner
        timerEnabled: true
        timerShowTime: 3000
        z:2
        parent: loginPage
    }

    Connections {
        target: Controller

        onGet_ticket_ok: {
            ma_waiting.close()

            webView.url = auth_url
            sheet_for_auth_via_box_net.open()
        }

        onError_on_get_ticket: {
            msgBox.caption = qsTr("Error")
            msgBox.title = qsTr("Can not get a ticket from Box.net server")
            msgBox.visible = true
            ma_waiting.close()
        }

        onGet_user_object_ok : {
            ma_waiting.close()
            bt_auth_via_box.visible = true
            pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }

        onError_on_get_user_object : {
            msgBox.caption = qsTr("Error")
            msgBox.title = qsTr("Can not verify with Box.net server")
            msgBox.visible = true
            ma_waiting.close()
        }

        onRegister_new_user_error : {
            ma_waiting.close()
            infoBanner.text = errStr
            infoBanner.show()
        }

        onRegister_new_user_ok : {
            ma_waiting.close()
            bt_auth_via_box.visible = true
            pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }

        onError_on_network: ma_waiting.close()

    } // end Connections

    Component.onCompleted: {
        theme.inverted = true
    }

    /*!
      Internal functions
      */

    function cmd_authenticate_via_box_net() {
        ma_waiting.open()
        Controller.auth_get_ticket()
    }

    function cmd_verify() {
        ma_waiting.open()
        Controller.auth_verify()
    }

    function cmd_register() {
        var errStr = ""
        if (Utils.is_email(tfEmail.text)) {
            if (tfPassword.text === tfConfirmPassword.text) {
                cmd_open_privacy_policy()
                isRegistering = true
            } else
                errStr = qsTr("Please enter a valid password")
        } else
            errStr = qsTr("Please enter a valid email address")

        if (errStr) {
            infoBanner.text = errStr
            infoBanner.show()
        }
    }

    function cmd_open_privacy_policy() {
        sheet_for_privacy_policy.url = "http://www.box.com/static/html/privacy.html"
        sheet_for_privacy_policy.open()
    }

    function cmd_continue_doing_something() {
        if(isRegistering) {
            ma_waiting.open()
            Controller.register_new_user(tfEmail.text, tfPassword.text)
            isRegistering = false
        }
    }

    function regTextChanged() {
        var email = Utils.trim11(tfEmail.text)
        var password = Utils.trim11(tfPassword.text)
        var confirmpassword = Utils.trim11(tfConfirmPassword.text)
        btRegister.enabled = email.length && password.length>=6 && confirmpassword.length>=6
    }
}

import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: fielboxPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: "box"; text_bold : true; show_menu: false}

    tools: commonTools

    property bool fromDirect : true

    Rectangle {
        id: title
        y: header.height
        width: fielboxPage.width
        height: 60
        color: "#ccd7e4"

        Row {
            anchors.fill: parent
            spacing: 5
            Rectangle {
                width: 60
                height: 60
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    source: "images/back.png"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked:  fileModel.back_to_parent_folder()
                    onPressed: parent.color = "#8BADB0"
                    onReleased: parent.color= "transparent"
                    onCanceled: parent.color= "transparent"
                }
            }

            Label {
                anchors.verticalCenter: parent.verticalCenter
                text: fromDirect?qsTr("Choose files to upload"):qsTr("Upload new version")
                font.bold: true
                font.pixelSize: 26
                color: "black"
            }
        }

        z:1
    }

    Rectangle {
        id:caption
        y: header.height + title.height
        width: parent.width
        height: 40
        color:"#F0C5EB"
        z:1

        property alias text: caption_label.text

        Label {
            id:caption_label
            x:10
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            font.pixelSize: 20
        }
    }

    /*! --------- */

    property Component fileListDelegate: Component {
        Rectangle {
            id: recItemDel
            width: fielboxPage.width
            height: 75
            color: (!model.is_folder && model.is_selected)?"white":"transparent"

            Rectangle {
                y: recItemDel.height - 1
                height: 1
                width: recItemDel.width
                color: "grey"
                z:1
            }

            Row {
                spacing: 10
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    Rectangle {
                        visible: !model.is_folder && fromDirect
                        x: 5
                       width: 5
                        height: 65
                        color: (!model.is_folder && model.is_selected)?"blue":"grey"
                    }
                }

                Column {
                    width: 69
                    Image {
                        x:5
                        width: 64
                        height: 64
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: model.is_folder?"images/folder.png":"images/file.png"
                    }
                }

                Column {
                    spacing: 3
                    Label {
                        text: model.name
                        color: "#395994"
                        width: recItemDel.width - 100
                        elide: Text.ElideRight
                        font.bold: model.is_folder
                        font.pixelSize: 28
                    }

                    Row {
                        spacing: 4
                        Column {
                            Text {
                                text:  Controller.get_locale_date(model.updated)
                                color: "gray"
                                font.pixelSize: 20
                            }
                        }
                        Column {
                            Text {
                                text: !model.is_folder? Utils.bytes_exchange(model.size):""
                                font.pixelSize: 20
                            }
                        }
                    }

                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (model.is_folder)
                        fileModel.change_folder(model.name)
                    else {
                        if (fromDirect) {
                            fileModel.folder_selected(model.index)
                            recItemDel.color = model.is_selected ? "white" : "transparent"
                        }
                    }
                }
                onPressed : recItemDel.color = "#E8CAD6"
                onReleased: recItemDel.color = (!model.is_folder && model.is_selected)?"white":"transparent"
                onCanceled: recItemDel.color = (!model.is_folder && model.is_selected)?"white":"transparent"
            }
        }
    }

    ListView {
        id: fileList
        visible: true
        anchors.fill: parent
        anchors.topMargin: header.height + title.height + caption.height
        model: fileModel
        delegate: fileListDelegate
    }

    /*! --------- */

    //-------------- Rec for Empty Folder -----------

    Rectangle {
        id:recEmptyFolder
        visible: false
        anchors.fill: parent
        color: "transparent"
        Label {
            anchors.centerIn: parent
            text : qsTr("This folder is empty")
            font.bold: false
            width: parent.width
            font.pixelSize: 68
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color:"#E8B7CB"
        }
    }

    //-------------- End Rec for Empty Folder ------

    ToolBarLayout {
        id: commonTools
        Row {
            anchors.centerIn: parent
            spacing: 30
            ToolButton {
                id:btn_choose
                visible: fromDirect
                enabled: false
                platformStyle: btplatformStyle
                text: qsTr("Choose")
                font.pixelSize: 22
                onClicked: {
                    pageStack.pop()
                    vibration.start()
                    Controller.start_upload_files_selected()
                }
            }
            ToolButton {
                text: qsTr("Cancel")
                onClicked: pageStack.pop()
                font.pixelSize: 22
            }
        }
    }

    Component.onCompleted: {
        fileModel.reset_all()
        fileModel.update_current_folder()
    }

    Connections {
        target: fileModel
        onFolderChanged: {
            caption.text = fileModel.get_current_folder()
            recEmptyFolder.visible = is_empty
        }
        onFolder_selected_ok: {
            btn_choose.enabled = has_selected
        }
    }
}

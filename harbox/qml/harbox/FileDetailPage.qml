import QtQuick 1.1
import com.nokia.meego 1.1
import com.meego.extras 1.0

import "harbox_utils.js" as Utils

Page {

    id: fileDetailPage

    orientationLock: PageOrientation.LockPortrait

    property string filename:""
    property string updated:""
    property string size:""
    property string url:""    
    property QtObject obj

    property bool is_renamed: false

    Header { id:header;title: "box"; text_bold : true; show_menu: false}

    Image {
        id: image
        y: 100
        width: parent.width
        height: 380
        anchors.horizontalCenter: parent.horizontalCenter
        fillMode: Image.PreserveAspectFit
        source: url
        onStatusChanged: waitingArea.visible = !(image.status == Image.Ready)

        BusyIndicator {
            id: waitingArea
            anchors.centerIn: parent
            z:1
            running: true
            platformStyle: BusyIndicatorStyle { size: "large";  period: 400; spinnerFrames:"image://theme/spinner"}
        }
    }

    Label {
        y: 500
        anchors.horizontalCenter: parent.horizontalCenter
        text: filename
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideMiddle
        width: parent.width - 40
        font.bold: true
        font.pixelSize: 26
    }

    Row {
        y:540
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter
        Label {
            text: updated
            color: "grey"
            font.pixelSize: 22
        }
        Label {
            text: size
            font.pixelSize: 22
            font.bold: true
        }
    }

    Column {
        y: 590
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        Button {
            text:qsTr("Share")
            font.pixelSize: 22
            onClicked: obj.cmd_share_send_a_link(0)
        }
        Button {
            text:qsTr("Comments")
            font.pixelSize: 22
            onClicked: cmd_comments()
        }
    }

    InfoBanner {
        id: infoBanner
        timerEnabled: true
        timerShowTime: 3000
        z:2
        parent: fileDetailPage
    }

    Connections {
        target: Controller

        onRename_file_ok: {
            infoBanner.text = qsTr("Rename complete")
            infoBanner.show()
        }

        onError_on_rename_file: {
            infoBanner.text = Controller.get_current_error_string()
            infoBanner.show()
        }
    }

    tools: commonTools

    ToolBarLayout {
        id: commonTools

        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }

        ToolIcon {
            platformIconId: "toolbar-down"
            onClicked: cmd_download_file()
        }

        ToolIcon {
            platformIconId: "toolbar-up"
            onClicked: cmd_upload_to_replace_file()
        }

        ToolIcon {
            platformIconId: "toolbar-edit"
            onClicked: cmd_rename_file()
        }

        ToolIcon {
            platformIconId: "toolbar-delete"
            onClicked: cmd_delete_file()
        }

        ToolIcon {
            platformIconId: "toolbar-view-menu"
            onClicked:  (contextMenu.status === DialogStatus.Closed) ? contextMenu.open() : contextMenu.close()
        }
    }

    ContextMenu {
        id: contextMenu

        MenuLayout {
            MenuItem {text: qsTr("Copy"); onClicked: cmd_copy_file()}
            MenuItem {text: qsTr("Move"); onClicked: cmd_move_file()}
        }
    }

    Connections {
        target: Controller
        onGet_account_tree_ok: {
            if (is_renamed) {
                var info = Controller.get_file_info_after_renamed()
                filename = info[0]
                updated = Controller.get_locale_date(info[1])
                size = Utils.bytes_exchange(info[2])
                url = info[3]
            }
        }

        onRename_file_ok: is_renamed = true
        onError_on_rename_file: is_renamed = false
        onDelete_file_ok: pageStack.pop()
        onError_on_delete_file: {
            infoBanner.text = Controller.get_current_error_string()
            infoBanner.show()
        }
    }

    function cmd_rename_file() { obj.cmd_rename_file()}
    function cmd_download_file() {
        vibration.start()
        Controller.start_download_file_openned()
    }
    function cmd_upload_to_replace_file() {
        pageStack.push(Qt.resolvedUrl("FileBoxPage.qml"), {fromDirect:false})
    }
    function cmd_delete_file() { obj.cmd_delete_file()}
    function cmd_move_file() { obj.cmd_lock_current_item_openned(0, false); pageStack.pop()}
    function cmd_copy_file() { obj.cmd_lock_current_item_openned(0, true); pageStack.pop()}
    function cmd_comments() {   pageStack.push(Qt.resolvedUrl("CommentsPage.qml")) }
}

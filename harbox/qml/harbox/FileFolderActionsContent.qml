import QtQuick 1.1
import com.nokia.meego 1.1

Item {
    id: ffactionsContent

    anchors.fill: parent

    property int state: 0
    property alias textVal: tf.text
    property real object_id: 0

    signal textchanged()

    Rectangle {
        id: top_banner
        x: -1
        y: -1
        width: parent.width + 1
        height: 60
        color: "#395994"

        Label {
            id:h_title
            anchors.centerIn: parent
            text: ffactionsContent.state===0?qsTr("Create New Folder"):ffactionsContent.state===1?qsTr("Rename folder to"):qsTr("Rename file to")
            color: "#ffffff"
            font.pixelSize: 26
            font.bold: true
        }
    }

    Rectangle {
        y: top_banner.height
        width: parent.width
        height: 60
        color:"white"
        TextField {
            id: tf
            anchors.fill: parent
            font.pixelSize: 24
            placeholderText: !ffactionsContent.state?qsTr("New folder here"):""
            platformStyle: TextFieldStyle {
                background:""
                backgroundSelected: ""
            }
            onTextChanged: textchanged()
        }
    }

}

import QtQuick 1.1
import com.nokia.meego 1.1


import "harbox_utils.js" as Utils

Page {

    id: settingsPage

    orientationLock: PageOrientation.LockPortrait

    Header { id:header;title: qsTr("Settings"); text_bold : true; show_menu: false}

    property bool m_something_changed: false

    Flickable {
        id: fickableMain
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: loginZone.height + loginZone.y + 10

            Column {
                id: loginZone
                y: 80
                spacing: 20

                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("General")
                            x:10
                        }

                        Row {
                            spacing: 5
                            Column {
                                width:420
                                spacing: 5
                                Label {
                                    text: qsTr("API key")
                                    color: "#395994"
                                    x:30
                                    font.pixelSize: 20
                                }
                                TextField {
                                    id: tf_api_key
                                    x:30
                                    text: Settings.get_api_key()
                                    width: parent.width
                                    font.pixelSize: 20
                                }
                            }
                        }

                        Row {
                            spacing: 5
                            Column {
                                width:420
                                spacing: 5
                                Label {
                                    text: qsTr("Download folder")
                                    color: "#395994"
                                    x:30
                                    font.pixelSize: 20
                                }
                                TextField {
                                    id: tf_download_folder
                                    x:30
                                    text: Settings.get_box_local_path()
                                    width: parent.width
                                    font.pixelSize: 20
                                }
                            }
                        }

                    }
                }

                Row {
                    Column {
                        spacing: 10
                        width: fickableMain.width

                        Label {
                            text: qsTr("API Limit")
                            x:10
                        }

                        Label {
                            text: qsTr("Excessive use of the API either by HarBox particular API key or a user accessing Box via HarBox API key will cause HarBox app to be rate limited.\n\nOnce the limit is reached Harbox will be unable to perform any requests to Box server until the limit is reset.\n\nYou can create an API by your-self to solve this problem ;)")
                            width: parent.width - 50
                            wrapMode: Text.WordWrap
                            color: "#395994"
                            x: 30
                            font.pixelSize: 20
                        }
                    }
            }
        }
    }

    QueryDialog {
        id: queryDialog

        titleText: qsTr("Something changed")
        message: qsTr("Do you want to save your changes ?")

        acceptButtonText: qsTr("Save")
        rejectButtonText: qsTr("Cancel")

        onRejected: pageStack.pop()
        onAccepted: {
            Settings.store_api_key(tf_api_key.text)
            Settings.store_box_local_path(tf_download_folder.text)
            pageStack.pop()
        }

    }

    Connections {target: tf_api_key; onTextChanged: m_something_changed=true}
    Connections {target: tf_download_folder; onTextChanged: m_something_changed=true}

    tools: commonTools

    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: {
                if (m_something_changed)
                    queryDialog.open()
                else
                    pageStack.pop()
            }
        }
    }

}

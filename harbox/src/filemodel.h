/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef FILEMODEL_H
#define FILEMODEL_H

#include <QDir>
#include <QVector>
#include <QDateTime>
#include <QStringList>
#include <QAbstractListModel>

#define DEFAULT_FOLDER "/home/user/MyDocs"

class FileModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit FileModel(QObject *parent = 0);
    
    enum {
        NameRole = Qt::UserRole + 10,
        SizeRole,
        UpdatedRole,
        Is_FolderRole,
        Is_SelectedRole
    };

    struct FileItem {
        FileItem() {
            name = QString();
            size = 0;
            updated = 0;
            is_folder = false;
            is_selected = false;
        }

        QString name;
        quint32 size;
        quint32 updated;
        bool is_folder;
        bool is_selected;
    };

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    Q_INVOKABLE void update_current_folder();
    Q_INVOKABLE QString get_current_folder() const;
    Q_INVOKABLE void change_folder(const QString&);
    Q_INVOKABLE void back_to_parent_folder();
    Q_INVOKABLE void folder_selected(const int&);
    Q_INVOKABLE void reset_all();


signals:
    void folder_selected_ok(const bool &has_selected);
    void dataChanged(const int &rows);
    void folderChanged(const int &is_empty);
    
public slots:

private:
        QString m_current_folder;
        QVector<FileItem*> fileItems;
        QList<QString> fileSelectedItems;


friend class Controller;

};

#endif // FILEMODEL_H

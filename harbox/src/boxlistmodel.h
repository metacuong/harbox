/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BOXLISTMODEL_H
#define BOXLISTMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QStringList>

class BoxListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit BoxListModel(QObject *parent = 0);
    ~BoxListModel();

    /*!
      This is the temporary structure for a box item (folder, file and tag)
      */

    struct BoxItem {
        BoxItem() {
            id = 0;
            name = QString();
            created = 0;
            updated = 0;
            size = 0;
            file_count = 0;
            permissions = QString();
            user_id = 0;
            shared = 0;
            shared_link = QString();
            description = QString();
            preview_thumbnail = QString();
            small_thumbnail = QString();
            is_folder = false;
            is_tag = false;
            is_selected = false;
            is_locked = false;
        }

        qlonglong id;
        QString name;
        quint32 created;
        quint32 updated;
        quint32 size;
        quint32 file_count;
        QString permissions;
        quint32 user_id;
        quint32 shared;
        QString shared_link;
        QString description;
        QString preview_thumbnail;
        QString small_thumbnail;
        bool is_folder;
        bool is_tag;
        bool is_selected;
        bool is_locked;
    };
    
    enum {
        IdRole = Qt::UserRole + 10,
        NameRole,
        CreatedRole,
        UpdatedRole,
        SizeRole,
        File_CountRole,
        SharedRole,
        Preview_ThumbnailRole,
        Small_ThumbnailRole,
        Is_FolderRole,
        Is_TagRole,
        Is_SelectedRole,
        Is_Locked
    };

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    bool removeRows(int row, int count, const QModelIndex&);
    void appendRow(BoxItem*);

    void clear();

    Q_INVOKABLE void item_selected(const int&);
    Q_INVOKABLE void force_item_selected();
    Q_INVOKABLE void clearSelection();
    Q_INVOKABLE void clearItemsSelected();
    Q_INVOKABLE bool has_selected() const;
    Q_INVOKABLE void lock_items_selected();
    Q_INVOKABLE void folder_opened(const int&);
    Q_INVOKABLE void file_opened(const qlonglong&);
    Q_INVOKABLE QList<int> how_many_folders_files_selected() const;

signals:
    void selectedChange();
    void lockedChange();
    
public slots:    

private:
     QVector<BoxItem*> boxItems;
     QHash<qlonglong, BoxItem*> selectedBoxItems;

     qlonglong m_item_openned;

     QStringList fullPath;

     QList<QList<qlonglong> > pop_id_items_selected();

friend class Controller;
friend class BoxCache;
};

#endif // BOXLISTMODEL_H

/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "dinterface.h"

#include <QDebug>

DInterface::DInterface(QObject *parent) :
    QObject(parent)
{
    QDBusConnection::sessionBus().connect(R_SERVICE,R_PATH,R_INTERFACE,"transfers_completed",this,SIGNAL(transfers_completed()));
    QDBusConnection::sessionBus().connect(R_SERVICE,R_PATH,R_INTERFACE,"upload_completed",this,SIGNAL(upload_completed(QVariantList)));
}

void DInterface::maybeStopDaemon() {
    methodCall("stop",QList<QVariant>());
}

void DInterface::startTransfer() {
    methodCall("startTransfer",QList<QVariant>());
}

QVariantList DInterface::getAllLogs() const{
    QDBusReply<QList<QVariant> > reply= methodCall("getAllLogs",QList<QVariant>());
    return reply.value();
}

void DInterface::clearLogs() {
    methodCall("clearLogs",QList<QVariant>());
}

void DInterface::appendItem(QString filename, quint32 size, qlonglong file_id,  int transferType) {
    QList<QVariant> args;
    QVariantList kwargs;
    kwargs <<  filename << size << file_id;
    args << qVariantFromValue(kwargs) << qVariantFromValue(transferType);
    methodCall("appendTransferItems", args);
}

QDBusMessage DInterface::methodCall(QString method, QList<QVariant> args) const{
    QDBusMessage message = QDBusMessage::createMethodCall(R_SERVICE,R_PATH,R_INTERFACE, method);
    message.setArguments(args);
    return QDBusConnection::sessionBus().call(message);
}

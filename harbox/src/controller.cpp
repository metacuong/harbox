/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "controller.h"

#include <QDebug>

Controller::Controller(QObject *parent) :
    QObject(parent),
    boxListModel(new BoxListModel(this)),
    fileModel(new FileModel(this)),
    boxCache(new BoxCache(this)),
    m_networkaccessmanager(new QNetworkAccessManager(this)),
    m_boxapi(new BoxApi(this, boxCache))
{
    QObject::connect(m_networkaccessmanager, SIGNAL(finished(QNetworkReply*)), SLOT(finished(QNetworkReply*)));
    QObject::connect(m_networkaccessmanager, SIGNAL(networkSessionConnected()), SLOT(networkSessionConnected()));   

    QObject::connect(&dInterface, SIGNAL(upload_completed(QVariantList)), SLOT(upload_completed(QVariantList)));

    network_is_busy = false;

}

Controller::~Controller(){
    dInterface.maybeStopDaemon();

    delete boxCache;
    delete fileModel;
    delete boxListModel;
    delete m_boxapi;
    delete m_networkaccessmanager;
}

void Controller::auth_get_ticket() {
    m_state = GET_TICKET;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::auth_authenticate() {
    m_state = USER_AUTHENTICATION;   
    emit get_ticket_ok(QString("%1/%2")
                            .arg(AUTH_URL)
                            .arg(m_boxapi->m_ticket));
}

void Controller::auth_verify() {
    m_state = VERIFY;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::finished(QNetworkReply *networkReply){
    bool finished_request = true;

    if (!networkReply->error()) {
        emit network_ok();
        switch (m_state) {
            case GET_TICKET:
                if (m_boxapi->get_ticket(networkReply))
                    auth_authenticate();
                else
                    emit error_on_get_ticket();
                break;
            case USER_AUTHENTICATION: break;
            case VERIFY:
                if(m_boxapi->get_user_object(networkReply))
                    emit get_user_object_ok();
                else
                    emit error_on_get_user_object();
                break;
            case LOGOUT:
                if (m_boxapi->logout(networkReply)) {
                    emit logout_ok();
                    reset_Everything();
                } else
                    emit error_on_logout();
                break;
            case GET_ACC_TREE:
                if (m_boxapi->get_account_tree(networkReply)) {
                    reload_boxListModel();                
                }
                else {
                    error_occured();
                    emit error_on_get_account_tree();
                }
                break;
            case CREATE_FOLDER:
                if(m_boxapi->create_folder(networkReply)) {
                    /*! FIXME : just append this item to cache instead of refresh cache */
                    finished_request = false;
                    update_current_folder();
                    emit create_folder_ok();
                }
                else {
                    error_occured();
                    emit error_on_create_folder();
                }
                break;
            case RENAME_FOLDER:
                if(m_boxapi->rename(networkReply)) {
                    /*! FIXME : just append this item to cache instead of refresh cache */
                    update_parrent_folder_cache();
                    emit rename_folder_ok();
                }
                else {
                    error_occured();
                    emit error_on_rename_folder();
                }
                break;
            case RENAME_FILE:
                if(m_boxapi->rename(networkReply)) {
                    finished_request = false;
                    update_current_folder();
                    emit rename_file_ok();
                }
                else {
                    error_occured();
                    emit error_on_rename_file();
                }
                break;
            case SINGLE_FOLDER_DELETE:
                if (m_boxapi->__delete(networkReply)) {
                    finished_request = false;
                    boxCache->flush_folder_id(m_boxapi->m_current_folder_id);
                    back_to_parent_folder_and_flush_cache();
                    emit delete_current_folder_ok();
                } else
                    emit error_on_delete_folder();
                break;
            case SINGLE_FILE_DELETE:
                if (m_boxapi->__delete(networkReply)) {
                    finished_request = false;
                    update_current_folder();
                    emit delete_file_ok();
                } else
                    emit error_on_delete_file();
                break;
            case MULTI_DELETE:
                finished_request = false;
                delete_one_of_those_items_finished();
                break;
            case COPY:
                m_boxapi->copy(networkReply);
                move_or_copy_to_finished();
                break;
            case MOVE:
                m_boxapi->move(networkReply);
                move_or_copy_to_finished();
                break;
            case PUBLISH_SHARE:
                if (m_boxapi->publish_share(networkReply))
                    emit created_share_link(m_boxapi->m_share_link);
                break;
            case PUBLISH_UNSHARE: break;
            case GET_ACC_INFO:
                emit get_acc_info_ok(m_boxapi->get_account_info(networkReply));
                break;
            case GET_COMMENTS:
                emit get_comments_ok(m_boxapi->get_comments(networkReply));
                break;
            case GET_UPDATES:
                m_boxapi->get_updates(networkReply);
                break;
            case REG_NEW_USER:
                if (m_boxapi->register_new_user(networkReply))
                    emit register_new_user_ok();                
                else
                    emit register_new_user_error(m_boxapi->errorString);
                break;
        }
    }else{

        switch (m_state) {
            case GET_ACC_TREE:
                boxListModel->clear();
                emit error_on_get_account_tree();
                break;
            case MULTI_DELETE:
                delete_one_of_those_items_finished();
            case COPY:
                move_or_copy_to_finished();
                break;
            case MOVE:
                move_or_copy_to_finished();
                break;
            default: break;
        }

        emit error_on_network(networkReply->errorString());
    }

    if(finished_request) {
        network_is_busy = false;
        emit finished_Request();
    }
}

void Controller::networkSessionConnected() {

}

QNetworkRequest Controller::buildRESTRequest() {
    emit start_Request();
    network_is_busy = true;

    QUrl url(REST_URL);
    QNetworkRequest networkRequest;

    switch(m_state) {
        case GET_TICKET:
            m_boxapi->get_ticket_params(&url);
            break;
        case USER_AUTHENTICATION: break;
        case VERIFY:
            m_boxapi->get_auth_token_params(&url);
            break;
        case LOGOUT:
            m_boxapi->logout_params(&url);
            break;
        case GET_ACC_TREE:
            m_boxapi->get_account_tree_params(&url);
            break;
        case CREATE_FOLDER:
            m_boxapi->create_folder_params(&url);
            break;
        case RENAME_FOLDER:
            m_boxapi->rename_params(&url, "folder");
            break;
        case RENAME_FILE:
            m_boxapi->rename_params(&url, "file");
            break;
        case SINGLE_FOLDER_DELETE:
            m_boxapi->delete_params(&url, "folder");
            break;
        case SINGLE_FILE_DELETE:
            m_boxapi->delete_params(&url, "file");
            break;
        case MULTI_DELETE:
            m_boxapi->delete_params(&url,
                                    m_multi_delete_items_id.back()[1]?"folder":"file",
                                    m_multi_delete_items_id.back()[0]);
            if (m_multi_delete_items_id.back()[1])
                boxCache->flush_folder_id(m_multi_delete_items_id.back()[0]);
            m_multi_delete_items_id.pop_back();
            break;
        case COPY:
            m_boxapi->copy_params(&url,
                                  m_items_to_move_or_copy.back()[0].toLongLong());
            m_items_to_move_or_copy.pop_back();
            break;
        case MOVE:
            m_boxapi->move_params(&url,
                                    m_items_to_move_or_copy.back()[1].toBool()?"folder":"file",
                                    m_items_to_move_or_copy.back()[0].toLongLong()
                                    );
            m_items_to_move_or_copy.pop_back();
            break;
        case GET_ACC_INFO:
            m_boxapi->get_account_info_params(&url);
            break;
        case PUBLISH_SHARE:
            m_boxapi->publish_share_params(&url, m_targetType);
            break;
        case PUBLISH_UNSHARE:
            m_boxapi->publish_unshare_params(&url, m_targetType);
            break;
        case GET_COMMENTS:
            m_boxapi->get_comments_params(&url);
            break;
        case REG_NEW_USER :
            m_boxapi->register_new_user_params(&url, m_email_to_reg, m_password_to_reg);
            break;
        case GET_UPDATES:
            QDate qDate(QDate::currentDate());
            QDateTime startTime(qDate, QTime(0, 0, 0, 0), Qt::UTC);
            QDateTime endTime(qDate, QTime(23, 59, 59, 0), Qt::UTC);
            m_boxapi->get_updates_params(&url, startTime.toTime_t() , endTime.toTime_t());
            break;
    }

    networkRequest.setUrl(url);
    return networkRequest;
}

void Controller::get_account_tree(const qlonglong &folder_id) {
    m_boxapi->m_parent_folder_id.append(m_boxapi->m_current_folder_id);
    reload_current_Folder(folder_id);
}

void Controller::logout() {
    m_state = LOGOUT;
    m_networkaccessmanager->get(buildRESTRequest());
}

bool Controller::client_is_authenticated() const {
    if (m_boxapi->m_auth_token.isEmpty() ||
            m_boxapi->m_ticket.isEmpty())
        return false;
    return true;
}

void Controller::reload_boxListModel() {
    boxListModel->clear();
    foreach(BoxListModel::BoxItem *item, boxCache->lookup(m_boxapi->m_current_folder_id)->boxItems)
            boxListModel->appendRow(item);

    emit get_account_tree_ok();
}

void Controller::update_current_folder() {
    boxCache->flush_folder_id(m_boxapi->m_current_folder_id);
    reload_current_Folder(m_boxapi->m_current_folder_id);
}

void Controller::update_parrent_folder_cache() {
    quint32 folder_id = m_boxapi->m_parent_folder_id.at(m_boxapi->m_parent_folder_id.count()-1);
    boxCache->flush_folder_id(folder_id);
    boxListModel->fullPath.pop_back();
    boxListModel->fullPath.append(m_boxapi->m_folder_or_filename);
}

void Controller::back_to_parent_folder() {
    quint32 folder_id = m_boxapi->m_parent_folder_id.takeAt(m_boxapi->m_parent_folder_id.count()-1);
    boxListModel->fullPath.pop_back();
    reload_current_Folder(folder_id);
}

void Controller::back_to_parent_folder_and_flush_cache() {
    quint32 folder_id = m_boxapi->m_parent_folder_id.takeAt(m_boxapi->m_parent_folder_id.count()-1);
    boxListModel->fullPath.pop_back();
    boxCache->flush_folder_id(folder_id);
    reload_current_Folder(folder_id);
}

bool Controller::back_parent_folder_is_root() const {
    quint32 folder_id = m_boxapi->m_parent_folder_id.takeAt(m_boxapi->m_parent_folder_id.count()-1);
    m_boxapi->m_parent_folder_id.append(folder_id);
    return folder_id == 0;
}

void Controller::reload_current_Folder(quint32 folder_id) {
    m_state = GET_ACC_TREE;
    m_boxapi->m_current_folder_id = folder_id;
    if (boxCache->is_cached(folder_id))
        reload_boxListModel();
    else
        m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::get_account_info() {
    m_state = GET_ACC_INFO;
    m_networkaccessmanager->get(buildRESTRequest());
}

bool Controller::check_current_folder_is_empty() const {
    return !boxListModel->boxItems.count();
}

bool Controller::current_folder_is_root() const {
    /*! 0 : is root */
    return !m_boxapi->m_current_folder_id;
}

void Controller::lock_current_folder(const bool &cType) {
    /*! false : move | true : copy */
    m_lock_folder_for = cType;
    m_lock_folder_id = m_boxapi->m_current_folder_id;
}

void Controller::unlock_last_folder() {
   BoxListModel *lModel = boxCache->lookup(m_lock_folder_id);
   foreach(BoxListModel::BoxItem *item, lModel->boxItems)
       if (item->is_locked)
           item->is_locked = false;
   lModel->selectedBoxItems.clear();
}

/*!
    @def file_folder_actions_emit
    @param actionType
    0 : create new folder
    1 : rename folder to
    2 : rename file to
    @param textVal
    @param object_id
    folder_id or file_id
*/
void Controller::file_folder_actions_emit(const int &actionType, const QString &textVal, const qlonglong &object_id) {
    switch(actionType) {
        case 0: m_state = CREATE_FOLDER;break;
        case 1: m_state = RENAME_FOLDER;break;
        case 2: m_state = RENAME_FILE;break;
    }
    m_boxapi->m_folder_or_filename = textVal;
    m_boxapi->m_object_id = object_id;
    m_networkaccessmanager->get(buildRESTRequest());
}

QString Controller::get_current_error_string() const {
    return m_boxapi->errorString;
}

QString Controller::get_full_path() const {
    QList<QString>::iterator i;
    QString fullPath;
    for (i = boxListModel->fullPath.begin(); i != boxListModel->fullPath.end(); ++i)
        fullPath.append(" > ").append(*i);
    fullPath.prepend(QT_TR_NOOP("All Files"));
    return fullPath;
}

QString Controller::get_current_folder_name() const {
    return boxListModel->fullPath.last();
}

void Controller::delete_items_selected() {
    m_multi_delete_items_id = boxListModel->pop_id_items_selected();
    /*! kick start deleting */
    delete_one_of_those_items_finished();
}

void Controller::delete_one_of_those_items_finished() {
    if (m_multi_delete_items_id.count()) {
        m_state = MULTI_DELETE;
        m_networkaccessmanager->get(buildRESTRequest());
    } else {
        update_current_folder();
        emit multi_items_delete_done();
    }
}

void Controller::delete_current_folder() {
    m_state = SINGLE_FOLDER_DELETE;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::delete_current_file() {
    m_state = SINGLE_FILE_DELETE;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::error_occured() {
    if (m_boxapi->errorCode == E_FOLDER_ID) {
        m_boxapi->m_current_folder_id = 0;
        boxCache->flush();
        boxListModel->fullPath.clear();
        update_current_folder();
    }
}

void Controller::change_object_id(const qlonglong &object_id) {
    m_boxapi->m_object_id = object_id;
}

QList<QVariant> Controller::get_file_info_after_renamed() const {
    foreach(BoxListModel::BoxItem *item, boxListModel->boxItems)
        if (item->id == m_boxapi->m_object_id)
            return QList<QVariant>() << item->name << item->updated << item->size << item->preview_thumbnail;
    return QList<QVariant>();
}

QString Controller::get_locale_date(const quint32 &timestamp) const {
    QDateTime qDateTime = QDateTime::fromTime_t(timestamp);
    return qDateTime.toString(qLocale.dateFormat(QLocale::LongFormat));
}

void Controller::start_download_file_openned() {
    foreach(BoxListModel::BoxItem *item, boxListModel->boxItems)
        if (item->id == boxListModel->m_item_openned) {
            dInterface.appendItem(item->name, item->size, boxListModel->m_item_openned, 0);
        }
}

void Controller::start_upload_files_selected() {
    foreach(QString filePath, fileModel->fileSelectedItems) {
         QFileInfo fileInfo(filePath);
         dInterface.appendItem(filePath, fileInfo.size(), m_boxapi->m_current_folder_id, 1);
    }
}

QVariantList Controller::getAllLogs() const {
    return dInterface.getAllLogs();
}

void Controller::clearLogs() {
    dInterface.clearLogs();
}

void Controller::getComments() {
    m_state = GET_COMMENTS;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::getUpdates() {
    m_state = GET_UPDATES;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::create_share_link(const int &targetType) {
    m_state = PUBLISH_SHARE;
    m_targetType = targetType;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::unshare(const int &targetType) {
    m_state = PUBLISH_UNSHARE;
    m_targetType = targetType;
    m_networkaccessmanager->get(buildRESTRequest());
}

/*!
  @def uploadDone
  transfer daemon will emit upload_completed signal after get all files uploaded
  */
void Controller::upload_completed(const QVariantList& folders_id) {
    foreach(QVariant folder_id, folders_id) {
        if (m_boxapi ->m_current_folder_id == folder_id.toLongLong())
            /*! if network is not busy then do update current folder
                else just do flush from the cache */
            if (!network_is_busy)
                update_current_folder();
            else
                boxCache->flush_folder_id(folder_id.toLongLong());
        else
            boxCache->flush_folder_id(folder_id.toLongLong());
    }
}

void Controller::move_or_copy_to() {
    if (m_lock_folder_id == m_boxapi->m_current_folder_id)
        emit select_another_folder();
    else {
        BoxListModel *lModel = boxCache->lookup(m_lock_folder_id);
        foreach(BoxListModel::BoxItem *item, lModel->boxItems)
            if (item->is_locked)
                m_items_to_move_or_copy <<
                                           (QVariantList() << QVariant::fromValue(item->id) << QVariant::fromValue(item->is_folder));

        m_state = m_lock_folder_for ? COPY : MOVE;
        move_or_copy_to_finished();
    }
}

void Controller::move_or_copy_to_finished() {
    if (m_items_to_move_or_copy.count()) {
        m_networkaccessmanager->get(buildRESTRequest());
    }else {
        boxCache->flush_folder_id(m_lock_folder_id);
        boxCache->flush_folder_id(m_boxapi->m_current_folder_id);
        update_current_folder();
        emit move_or_copy_finished();
    }
}

void Controller::register_new_user(const QString &email, const QString &password) {
    m_state = REG_NEW_USER;
    m_email_to_reg = email;
    m_password_to_reg = password;
    m_networkaccessmanager->get(buildRESTRequest());
}

void Controller::reset_Everything() {
    /*! Remove auth_token and ticket from setting */
    m_settings.store_auth_token("");
    m_settings.store_ticket("");
    /*! Clear boxcache */
    boxCache->flush();
    /*! Clear current boxlistmodel */
    boxListModel->clear();
    m_boxapi->m_current_folder_id = 0;
    m_boxapi->m_parent_folder_id.clear();
}

QString Controller::applicationVersion() const {
    return QString("%1.%2.%3").arg(APPVERSION_MAJOR).arg(APPVERSION_MINOR).arg(APPVERSION_PATCH);
}

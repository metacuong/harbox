/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "settings.h"
#include "global.h"

Settings::Settings(QObject *parent)
    :QObject(parent)
{
}

QString Settings::get_api_key() {
    return sGet("auth_info", "api_key", API_KEY).toString();
}

QString Settings::get_ticket() {
    return sGet("auth_info", "ticket", QString()).toString();
}

QString Settings::get_auth_token() {
    return sGet("auth_info", "auth_token", QString()).toString();
}

QString Settings::get_box_local_path() {
    QString pPath = sGet("general", "box_local_path", QString()).toString();
    if (pPath.isEmpty())
        pPath = DEFAULT_BOX_LOCAL_PATH;
    return pPath;
}

void Settings::store_ticket(QString val) {
    sStore("auth_info", "ticket", val);
}

void Settings::store_auth_token(QString val) {
    sStore("auth_info", "auth_token", val);
}

void Settings::store_api_key(QString val) {
    sStore("auth_info", "api_key", val);
}

void Settings::store_box_local_path(QString val) {
    sStore("general", "box_local_path", val);
}


QVariant Settings::sGet(QString groupName, QString keyName, QVariant defaultVal) {
    QSettings settings;
    settings.beginGroup(groupName);
    if (settings.childKeys().indexOf(keyName) == -1)
        return defaultVal;
    return settings.value(keyName);
}

void Settings::sStore(QString groupName, QString keyName, QVariant val) {
    QSettings settings;
    settings.setValue(QString("%1/%2").arg(groupName).arg(keyName), val);
}

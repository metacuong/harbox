/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GLOBAL_H
#define GLOBAL_H

#define API_KEY "gd643dexx6mn48nc1yp5tmy7ga9uuzhf"

#define REST_URL "https://www.box.com/api/1.0/rest"
#define AUTH_URL "https://m.box.com/api/1.0/auth"

#define DOWNLOAD_URL "https://www.box.net/api/1.0"
#define UPLOAD_URL "https://upload.box.net/api/1.0"

//OAuth version 2

#define CLIENT_ID "gd643dexx6mn48nc1yp5tmy7ga9uuzhf"
#define CLIENT_SECRET "pt5Z9c07sgJ0xFkNPqOoq4nNLgQGmyAG"

#define AUTH_URL_OAUTH2 "https://api.box.com/oauth2/authorize"
#define REDIRECT_URI "/harbox_authenticated_ok"

#define ACCESS_TOKEN_URL_OAUTH2 "https://api.box.com/oauth2/token"


/*
  Authentication
  */

#define A_GET_TICKET "get_ticket"
#define A_GET_AUTH_TOKEN "get_auth_token"
#define A_LOGOUT "logout"
#define A_REGISTER_NEW_USER "register_new_user"
#define A_REGISTER_NEW_USER_OK "successful_register"
#define A_VERIFY_REG_EMAIL "verify_registration_email"
#define A_VERIFY_REG_EMAIL_OK "email_ok"
#define A_GET_ACC_INFO "get_account_info"
#define A_GET_ACC_INFO_OK "get_account_info_ok"
#define A_LOGOUT_OK "logout_ok"

#define GET_TICKET_OK "get_ticket_ok"
#define GET_AUTH_TOKEN_OK "get_auth_token_ok"

/*
  File & Folder Operations
  */

#define FF_GET_ACC_TREE "get_account_tree"
#define FF_CREATE_FOLDER "create_folder"
#define FF_RENAME "rename"
#define FF_DELETE "delete"
#define FF_COPY "copy"
#define FF_MOVE "move"

#define FF_LISTING_OK "listing_ok"
#define FF_CREATE_FOLDER_OK "create_ok"
#define FF_RENAME_OK "s_rename_node"
#define FF_DELETE_OK "s_delete_node"
#define FF_COPY_OK "s_copy_node"
#define FF_MOVE_OK "s_move_node"

/*
  Sharing
  */

#define S_PUBLISH_SHARE "public_share"
#define S_PUBLISH_SHARE_OK "share_ok"
#define S_PUBLISH_UNSHARE "public_unshare"
#define S_PUBLISH_UNSHARE_OK "unshare_ok"
#define S_PUBLISH_PREFIX_URL "http://www.box.net/s"

/*
  Collaboration
  */

/*
  Contacts
  */

/*
  Tagging
  */

/*
  Commenting
  */

#define CM_GET_COMMENTS "get_comments"
#define CM_GET_COMMENTS_OK "get_comments_ok"

/*
  Version History
  */

#define VH_GET_VERSIONS "get_versions"

/*
  Miscellaneous
  */

#define M_GET_UPDATES "get_updates"
#define M_GET_UPDATES_OK "s_get_updates"

/*
  Errors code string
  */

#define E_FOLDER_ID "e_folder_id"

/*
  Internal macros
  */

#define GLOBAL_FONT "Nokia Pure Text"
#define ORGANIZATION_NAME "cuonglb"
#define APPLICATION_NAME "HarBox"

#define R_SERVICE "org.harboxd.filetransfer"
#define R_PATH "/harboxd/filetransfer"
#define R_INTERFACE R_SERVICE
#define R_OBJECT R_PATH

#define CLIENT_TRANSFER_TYPES_UPLOAD 0
#define CLIENT_TRANSFER_TYPES_DOWNLOAD 1

#define DEFAULT_BOX_LOCAL_PATH "/home/user/MyDocs/Box.net"
#define DEFAULT_TRANSFER_LOGS_PATH "/home/user/.cache/harbox"

#endif // GLOBAL_H

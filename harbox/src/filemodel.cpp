/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "filemodel.h"

#include <QDebug>

FileModel::FileModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[SizeRole] = "size";
    roles[UpdatedRole] = "updated";
    roles[Is_FolderRole] = "is_folder";
    roles[Is_SelectedRole] = "is_selected";
    setRoleNames(roles);    
}

QVariant FileModel::data(const QModelIndex &index, int role) const {
    switch(role) {
        case NameRole : return QVariant::fromValue(fileItems[index.row()]->name);break;
        case SizeRole : return QVariant::fromValue(fileItems[index.row()]->size);break;
        case UpdatedRole : return QVariant::fromValue(fileItems[index.row()]->updated);break;
        case Is_FolderRole : return QVariant::fromValue(fileItems[index.row()]->is_folder);break;
        case Is_SelectedRole : return QVariant::fromValue(fileItems[index.row()]->is_selected);break;
        default:
            return QVariant();
    }
}

int FileModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return fileItems.count();
}

void FileModel::update_current_folder() {
    QStringList m_folder_content = QDir(m_current_folder)
            .entryList(QDir::AllEntries | QDir::NoDot | QDir::NoDotDot | QDir::Readable, QDir::DirsFirst| QDir::Name);

    fileItems.clear();
    reset();

    foreach(QString name, m_folder_content) {
        FileItem *item = new FileItem();
        QString pPath = QString("%1/%2").arg(m_current_folder).arg(name);
        QFileInfo fileInfo(pPath);
        item->name = name;
        item->is_folder = fileInfo.isDir();
        item->updated = fileInfo.lastModified().toTime_t();
        if (!item->is_folder) {
            item->size = fileInfo.size();
            if (fileSelectedItems.contains(pPath))
                item->is_selected = true;
        }
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        fileItems << item;
        endInsertRows();
    }

    emit dataChanged(rowCount());
    emit folderChanged(m_folder_content.isEmpty());
}

QString FileModel::get_current_folder() const {
    return m_current_folder;
}

void FileModel::change_folder(const QString &folder_name) {
    m_current_folder = QString(m_current_folder == "/"?"%1%2":"%1/%2")
            .arg(m_current_folder)
            .arg(folder_name);
    update_current_folder();
}

void FileModel::back_to_parent_folder() {
    QDir qDir = QDir(m_current_folder);
    qDir.cdUp();
    m_current_folder = qDir.path();
    update_current_folder();
}

void FileModel::folder_selected(const int &index) {
    emit layoutAboutToBeChanged();
    if (index >= 0 && index < rowCount()) {
        fileItems[index]->is_selected = !fileItems[index]->is_selected;
        QString pPath = QString(m_current_folder == "/"?"%1%2":"%1/%2")
                .arg(m_current_folder)
                .arg(fileItems[index]->name);
        if (fileItems[index]->is_selected)
            fileSelectedItems.append(pPath);
        else {
            int sIndex = fileSelectedItems.indexOf(pPath);
            if (sIndex >= 0)
                fileSelectedItems.removeAt(sIndex);
        }
    }
    emit layoutChanged();
    emit folder_selected_ok(fileSelectedItems.count());
}

void FileModel::reset_all() {
    m_current_folder = DEFAULT_FOLDER;
    fileSelectedItems.clear();
}

/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "boxlistmodel.h"

#include <QDebug>

BoxListModel::BoxListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[CreatedRole] = "created";
    roles[UpdatedRole] = "updated";   
    roles[SizeRole] = "size";
    roles[File_CountRole] = "file_count";
    roles[SharedRole] = "shared";
    roles[Preview_ThumbnailRole] = "preview_thumbnail";
    roles[Small_ThumbnailRole] = "small_thumbnail";
    roles[Is_FolderRole] = "is_folder";
    roles[Is_TagRole] = "is_tag";
    roles[Is_SelectedRole] = "is_selected";
    roles[Is_Locked] = "is_locked";
    setRoleNames(roles);
}

BoxListModel::~BoxListModel()  {

}

QVariant BoxListModel::data(const QModelIndex &index, int role) const {
    switch(role) {
        case IdRole : return QVariant::fromValue(boxItems[index.row()]->id);break;
        case NameRole : return QVariant::fromValue(boxItems[index.row()]->name);break;
        case CreatedRole : return QVariant::fromValue(boxItems[index.row()]->created);break;
        case UpdatedRole : return QVariant::fromValue(boxItems[index.row()]->updated);break;
        case SizeRole : return QVariant::fromValue(boxItems[index.row()]->size);break;
        case File_CountRole : return QVariant::fromValue(boxItems[index.row()]->file_count);break;
        case SharedRole: return QVariant::fromValue(boxItems[index.row()]->shared);break;
        case Preview_ThumbnailRole: return QVariant::fromValue(boxItems[index.row()]->preview_thumbnail);break;
        case Small_ThumbnailRole: return QVariant::fromValue(boxItems[index.row()]->small_thumbnail);break;
        case Is_FolderRole : return QVariant::fromValue(boxItems[index.row()]->is_folder);break;
        case Is_TagRole : return QVariant::fromValue(boxItems[index.row()]->is_tag);break;
        case Is_SelectedRole : return QVariant::fromValue(boxItems[index.row()]->is_selected);break;
        case Is_Locked : return QVariant::fromValue(boxItems[index.row()]->is_locked);break;
        default:
            return QVariant();
    }
}

int BoxListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return boxItems.count();
}

bool BoxListModel::removeRows(int row, int count, const QModelIndex &parent) {
    Q_UNUSED(count);
    beginRemoveRows(parent, row, row);
    boxItems.remove(row);
    endRemoveRows();
    return true;
}

void BoxListModel::appendRow(BoxItem *item) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    boxItems << item;
    endInsertRows();
}

void BoxListModel::item_selected(const int &index) {
    emit layoutAboutToBeChanged();
    if (index >= 0 && index < rowCount()) {
        boxItems[index]->is_selected = !boxItems[index]->is_selected;
        /*! puts all selected item to QHash
             and it will be cleared after
             transfer daemon/controller rest action receive them */
        if (boxItems[index]->is_selected)
            selectedBoxItems.insert(boxItems[index]->id, boxItems[index]);
        else
            selectedBoxItems.remove(boxItems[index]->id);
    }
    emit layoutChanged();
}

void BoxListModel::clearSelection() {
    emit layoutAboutToBeChanged();
    foreach(BoxItem *item, boxItems) {
        if (item->is_selected) {
            item->is_selected = false;
            item->is_locked = false;
        }
    }
    emit layoutChanged();
    emit selectedChange();
}

void BoxListModel::clearItemsSelected() {
    selectedBoxItems.clear();
    emit layoutAboutToBeChanged();
    foreach(BoxItem *item, boxItems) {
        if (item->is_locked) {
            item->is_locked = false;
        }
    }
    emit layoutChanged();
    emit lockedChange();
}

void BoxListModel::lock_items_selected() {
    emit layoutAboutToBeChanged();
    foreach(BoxItem *item, boxItems) {
        if (item->is_selected) {
            item->is_selected = false;
            item->is_locked = true;
        }
    }
    emit layoutChanged();
    emit lockedChange();
}

bool BoxListModel::has_selected() const {
    return selectedBoxItems.count();
}

void BoxListModel::clear() {
    beginResetModel();
    boxItems.clear();
    selectedBoxItems.clear();
    reset();
    endResetModel();
}

void BoxListModel::folder_opened(const int &index) {
    if (index >= 0 && index < rowCount()) {
        fullPath.append(boxItems[index]->name);
        m_item_openned = boxItems[index]->id;
    }
}

void BoxListModel::file_opened(const qlonglong &file_id) {
     m_item_openned = file_id;
}

void BoxListModel::force_item_selected() {
    emit layoutAboutToBeChanged();
    foreach(BoxItem *item, boxItems) {
        if (item->id == m_item_openned) {
            item->is_selected = false;
            item->is_locked = true;
        }
    }
    emit layoutChanged();
    emit lockedChange();
}

QList<QList<qlonglong> > BoxListModel::pop_id_items_selected() {
    QList<QList<qlonglong> > rawIds;
    foreach(BoxItem *item, selectedBoxItems) {
        QList<qlonglong> qL;
        qL << item->id << item->is_folder;
        rawIds << qL;
    }
    clearSelection();
    selectedBoxItems.clear();
    return rawIds;
}

QList<int> BoxListModel::how_many_folders_files_selected() const {
    QList<int> ffSelected;
    int cFolders = 0;
    int cFiles = 0;
    foreach(BoxItem *item, selectedBoxItems)
        if (item->is_folder)
                cFolders++;
        else
                cFiles++;
    ffSelected << cFolders << cFiles;
    return ffSelected;
}

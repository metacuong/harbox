/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BOXCACHE_H
#define BOXCACHE_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QHash>

#include <QtXml>

#include "boxlistmodel.h"

/*!
  Define the abs path to store all cache files.
  This cache path will be created/removed by debian 'preinst' and 'prerm' script
  */
#define CACHE_PATH "/home/user/.cache/harbox"

class BoxCache : public QObject
{
    Q_OBJECT
    Q_ENUMS(ElemType)
public:
    explicit BoxCache(QObject *parent = 0);
    ~BoxCache();

    enum ElemType{
        FILE,
        FOLDER,
        TAG
    };

    bool maybe_cache(quint32, QVector<BoxListModel::BoxItem*>);
    BoxListModel* lookup(quint32);
    bool is_cached(quint32);

    void flush();
    void flush_folder_id(quint32);

    BoxListModel::BoxItem* createItemCache(QDomElement, ElemType);
    
signals:
    
public slots:

private:
    bool ensure_cache_path_created();

    QHash<quint32, BoxListModel*> rawCache;
};

#endif // BOXCACHE_H

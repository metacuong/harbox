/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DINTERFACE_H
#define DINTERFACE_H

#include <QObject>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusPendingReply>
#include <QDBusReply>

#include "global.h"

class DInterface : public QObject
{
    Q_OBJECT
public:
    explicit DInterface(QObject *parent = 0);

    /*!
      filename, size, file_id, transfer type
      */
    void appendItem(QString, quint32, qlonglong, int);

    void maybeStopDaemon();
    void startTransfer();

    QVariantList getAllLogs() const;
    void clearLogs();

signals:
    void transfers_completed();
    void upload_completed(const QVariantList&);

    
public slots:
    
private:
    QDBusMessage methodCall(QString, QList<QVariant>) const;
};

#endif // DINTERFACE_H

/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "boxcache.h"

#include <QDebug>

BoxCache::BoxCache(QObject *parent) :
    QObject(parent)
{
    ensure_cache_path_created();
}

BoxCache::~BoxCache() {
}

 BoxListModel* BoxCache::lookup(quint32 folder_id) {
     if (is_cached(folder_id))
         return rawCache.value(folder_id);
    return new BoxListModel;
}

bool BoxCache::maybe_cache(quint32 folder_id, QVector<BoxListModel::BoxItem*> boxListItem) {
    if (!is_cached(folder_id) && !boxListItem.isEmpty()) {
        BoxListModel *newCache = new BoxListModel;
        foreach(BoxListModel::BoxItem *item, boxListItem) {
            newCache->appendRow(item);
        }
        rawCache.insert(folder_id, newCache);
        return true;
    }
    return false;
}

bool BoxCache::is_cached(quint32 folder_id) {
    if (!rawCache.isEmpty())
        if (rawCache.value(folder_id, 0)){
            return true;
        }
    return false;
}

bool BoxCache::ensure_cache_path_created() {
    QDir qDir;
    if (!qDir.exists(CACHE_PATH))
        return qDir.mkdir(CACHE_PATH);
    return true;
}

void BoxCache::flush() {
    rawCache.clear();
}

void BoxCache::flush_folder_id(quint32 folder_id) {
    rawCache.remove(folder_id);
}

BoxListModel::BoxItem* BoxCache::createItemCache(QDomElement elem, ElemType elemType) {
    BoxListModel::BoxItem *item = new BoxListModel::BoxItem();

    item->id = elem.attribute("id").toLongLong();

    item->name = elem.attribute(
                elemType == FOLDER?"name":(elemType == FILE?"file_name":"")
                                   );
    item->updated = elem.attribute("updated").toLong();
    item->created = elem.attribute("created").toLong();

    if (elemType == TAG) {
        item->is_folder = false;
        item->is_tag = true;
    }else {
        item->is_folder = elemType == FOLDER? true : false;
        item->is_tag = false;
        item->size = elem.attribute("size").toLong();
        if (elemType == FOLDER) {
            item->file_count = elem.attribute("file_count").toLong();
            item->shared = elem.attribute("shared").toLong();
        } else {
            //thumbnail | small_thumbnail | large_thumbnail | preview_thumbnail
            item->preview_thumbnail = elem.attribute("preview_thumbnail");
            item->small_thumbnail = elem.attribute("large_thumbnail");
        }
        item->permissions = elem.attribute("permissions");
    }        

    return item;
}

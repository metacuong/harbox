/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QSettings>

class Settings : public QObject
{
     Q_OBJECT
public:
    Settings(QObject *parent = 0);
    
    Q_INVOKABLE QString get_api_key();
    QString get_ticket();
    QString get_auth_token();
    Q_INVOKABLE QString get_box_local_path();

    Q_INVOKABLE void store_api_key(QString);
    void store_ticket(QString);
    void store_auth_token(QString);
    Q_INVOKABLE void store_box_local_path(QString);

private:

    QVariant sGet(QString, QString, QVariant);
    void sStore(QString, QString, QVariant);

};

#endif // SETTINGS_H

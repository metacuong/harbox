/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BOXAPI_H
#define BOXAPI_H

#include <QObject>
#include <QUrl>
#include <QMap>
#include <QtNetwork/QNetworkReply>
#include <QtXml/QDomDocument>

#include "settings.h"
#include "boxlistmodel.h"
#include "boxcache.h"

/*! This object is implemented Box.net REST API version 1.0
     All response body data is sent back as XML. */

/*! Notice : Box.net version 2.0 still in develop (it is in beta)
    They will have a plan to drop down version 1.0 when version 2.0 get stable release */

class BoxApi : public QObject
{
    Q_OBJECT
public:
    explicit BoxApi(QObject *parent = 0, BoxCache *boxCache = 0);
    ~BoxApi();

    qlonglong m_current_folder_id;
    QList<qlonglong> m_parent_folder_id;

    QString m_folder_or_filename;
    qlonglong m_object_id;

    QString m_share_link;

    QString errorString;
    QString errorCode;

    /*!
      Authentication
      */

    void get_ticket_params(QUrl*);
    bool get_ticket(QNetworkReply*);

    void get_auth_token_params(QUrl*);
    bool get_user_object(QNetworkReply*);

    void logout_params(QUrl*);
    bool logout(QNetworkReply*);

    void register_new_user_params(QUrl*, QString, QString);
    bool register_new_user(QNetworkReply*);

    void verify_registration_email_params(QUrl*, QString);
    bool verify_registration_email(QNetworkReply*);

    void get_account_info_params(QUrl*);
    QList<QVariant> get_account_info(QNetworkReply*);

    /*!
      File & Folder operations
      */

    void get_account_tree_params(QUrl*);
    bool get_account_tree(QNetworkReply*);

    void create_folder_params(QUrl*);
    bool create_folder(QNetworkReply*);

    void rename_params(QUrl*, QString);
    bool rename(QNetworkReply*);

    void delete_params(QUrl*, QString, qlonglong item_id = -1);
    bool __delete(QNetworkReply*);

    /*! @note
       Currently this can only be set to 'file'
       */
    void copy_params(QUrl*, qlonglong item_id = -1);
    bool copy(QNetworkReply*);

    void move_params(QUrl*, QString, qlonglong item_id = -1);
    bool move(QNetworkReply*);

    /*!
      Sharing
      */

    void publish_share_params(QUrl*, int);
    bool publish_share(QNetworkReply*);

    void publish_unshare_params(QUrl*, int);
    bool publish_unshare(QNetworkReply*);

    /*!
      Version History
      */

    void get_versions_params(QUrl*, qlonglong);
    bool get_versions(QNetworkReply*);

    /*!
      Commenting
      */

    void get_comments_params(QUrl*);
    QList<QVariant> get_comments(QNetworkReply*);

    /*!
      Miscellaneous
      */

    void get_updates_params(QUrl*, quint32, quint32);
    bool get_updates(QNetworkReply*);


    QString m_api_key;
    QString m_ticket;
    QString m_auth_token;

    QMap<QString, QString> m_user_object;
    
signals:
    
public slots:

private:
    QDomDocument *m_domdocument;
    BoxCache* m_boxCache;

    Settings m_settings;

    void getErrorString(QString);
    
};

#endif // BOXAPI_H

/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BOXUPDATESMODEL_H
#define BOXUPDATESMODEL_H

#include <QAbstractListModel>

class BoxUpdatesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit BoxUpdatesModel(QObject *parent = 0);
    
    enum Update_Type{
        ADDED,
        UPDATED
    };

    struct BoxUpdateItem {
        BoxUpdateItem() {
            id = 0 ;
            name = QString();
            updated_type = ADDED;
            by_user= QString();
            file_size = 0;
            updated = 0;
            file_count = 0;
            size = 0;
            is_folder = true;
            thumbnail_url = QString();
        }
        quint32 id;
        QString name;
        Update_Type updated_type;
        QString  by_user;
        quint32 file_size;
        quint32 updated;
        quint32 file_count;
        quint32 size;
        bool is_folder;
        QString thumbnail_url;
    };

signals:
    
public slots:
    
};

#endif // BOXUPDATESMODEL_H

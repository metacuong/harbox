/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QDesktopServices>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <QLocale>

#include "global.h"
#include "settings.h"
#include "boxapi.h"

#include "boxlistmodel.h"
#include "filemodel.h"
#include "boxcache.h"

#include "dinterface.h"

class Controller : public QObject
{
    Q_OBJECT
    Q_ENUMS(State)
public:
    explicit Controller(QObject *parent = 0);
    ~Controller();

    enum State{
        /* Authenticate */
        GET_TICKET,
        USER_AUTHENTICATION,
        VERIFY,
        LOGOUT,
        GET_ACC_INFO,
        REG_NEW_USER,
        /* File & Folder */
        GET_ACC_TREE,
        CREATE_FOLDER,
        RENAME_FOLDER,
        RENAME_FILE,
        SINGLE_FOLDER_DELETE,
        SINGLE_FILE_DELETE,
        MULTI_DELETE,
        COPY,
        MOVE,
        /* Sharing */
        PUBLISH_SHARE,
        PUBLISH_UNSHARE,
        /* Commenting */
        GET_COMMENTS,
        /* Miscellaneous */
        GET_UPDATES
    };

    BoxListModel *boxListModel;
    FileModel *fileModel;
    BoxCache *boxCache;
    Settings m_settings;

signals:
    void error_on_get_ticket();
    void error_on_get_user_object();
    void error_on_logout();

    void error_on_get_account_tree();
    void error_on_create_folder();

    void get_ticket_ok(const QString auth_url);
    void get_user_object_ok();
    void logout_ok();
    void get_acc_info_ok(const QList<QVariant>& accInfo);

    void get_account_tree_ok();
    void create_folder_ok();

    void rename_folder_ok();
    void error_on_rename_folder();

    void rename_file_ok();
    void error_on_rename_file();

    void delete_current_folder_ok();
    void error_on_delete_folder();

    void delete_file_ok();
    void error_on_delete_file();

    void error_on_network(const QString& error_string);
    void network_ok();

    void multi_items_delete_done();

    void start_Request();
    void finished_Request();

    void created_share_link(const QString &share_link);

    void select_another_folder();
    void move_or_copy_finished();

    void register_new_user_error(const QString errStr);
    void register_new_user_ok();

    /*! FIXME : it should a QAbstractListModel */
    void get_comments_ok(const QList<QVariant> comments);

public slots:
    void auth_get_ticket();
    void auth_verify();
    void logout();
    void get_account_info();
    void register_new_user(const QString&, const QString&);

    void get_account_tree(const qlonglong &);

    bool client_is_authenticated() const;
    QString get_current_error_string() const;

    void update_current_folder();
    void update_parrent_folder_cache();
    void back_to_parent_folder();
    bool back_parent_folder_is_root() const;
    bool check_current_folder_is_empty() const;
    bool current_folder_is_root() const;        

    void lock_current_folder(const bool&);
    void unlock_last_folder();
    void file_folder_actions_emit(const int&, const QString&, const qlonglong&);
    void move_or_copy_to();
    void move_or_copy_to_finished();

    QString get_full_path() const;
    QString get_current_folder_name() const;

    QString get_locale_date(const quint32&) const;

    void delete_items_selected();
    void delete_current_folder();
    void delete_current_file();

    void start_download_file_openned();
    void start_upload_files_selected();
    void upload_completed(const QVariantList&);

    void change_object_id(const qlonglong &);

    QList<QVariant> get_file_info_after_renamed() const;

    QVariantList getAllLogs() const;
    void clearLogs();

    void getComments();
    void getUpdates();

    void create_share_link(const int&);
    void unshare(const int&);

    void networkSessionConnected();

    QString applicationVersion() const;


private slots:
    void finished(QNetworkReply*);
    void auth_authenticate();

private:
    QNetworkAccessManager *m_networkaccessmanager;
    BoxApi *m_boxapi;    
    State m_state;

    QList<QVariantList> m_items_to_move_or_copy;
    quint32 m_lock_folder_id;
    bool m_lock_folder_for;
    int m_targetType; /* for share/unshare link feature */

    QString m_email_to_reg;
    QString m_password_to_reg;

    QList<QList<qlonglong> > m_multi_delete_items_id;

    QNetworkRequest buildRESTRequest();

    QLocale qLocale;
    DInterface dInterface;

    bool network_is_busy;

    void reload_boxListModel();
    void reload_current_Folder(quint32);
    void reset_Everything();
    void error_occured();
    void back_to_parent_folder_and_flush_cache();
    void delete_one_of_those_items_finished();
};

#endif // CONTROLLER_H

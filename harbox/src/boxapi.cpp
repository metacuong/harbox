/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "boxapi.h"
#include "global.h"

#include <QDebug>

BoxApi::BoxApi(QObject *parent, BoxCache *boxCache) :
    QObject(parent),
    m_domdocument(new QDomDocument()),
    m_boxCache(boxCache)
{
    /*!
      Gets ticket and auth_token value from settings
      */
    m_api_key = m_settings.get_api_key();
    m_ticket = m_settings.get_ticket();
    m_auth_token = m_settings.get_auth_token();

    m_current_folder_id = 0;    
}

BoxApi::~BoxApi() {
    delete m_domdocument;
}

void BoxApi::get_ticket_params(QUrl *url){
    url->addQueryItem("action", A_GET_TICKET);
    url->addQueryItem("api_key", m_api_key);
}

bool BoxApi::get_ticket(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();
    if (!QString::compare(docElem.elementsByTagName("status").at(0).toElement().text(),
                         GET_TICKET_OK,
                         Qt::CaseSensitive)) {
        m_ticket = docElem.elementsByTagName("ticket").at(0).toElement().text();
        return true;
    }
    return false;
}

void BoxApi::get_auth_token_params(QUrl *url) {
    url->addQueryItem("action", A_GET_AUTH_TOKEN);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("ticket", m_ticket);
}

bool BoxApi::get_user_object(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();
    if (!QString::compare(docElem.elementsByTagName("status").at(0).toElement().text(),
                         GET_AUTH_TOKEN_OK,
                         Qt::CaseSensitive)) {
        m_auth_token = docElem.elementsByTagName("auth_token").at(0).toElement().text();

        m_settings.store_ticket(m_ticket);
        m_settings.store_auth_token(m_auth_token);

        QDomNodeList __list = docElem.elementsByTagName("user");
        for(uint i=0; i<__list.length();i++) {
            m_user_object[__list.at(i).toElement().tagName()] =
                    __list.at(i).toElement().text();
        }
        return true;
    }
    return false;
}

void BoxApi::logout_params(QUrl *url) {
    url->addQueryItem("action", A_LOGOUT);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
}

bool BoxApi::logout(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, A_LOGOUT_OK, Qt::CaseSensitive))
        return true;
    return false;
}

void BoxApi::register_new_user_params(QUrl *url, QString email_address, QString password) {
    url->addQueryItem("action", A_REGISTER_NEW_USER);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("login", email_address);
    url->addQueryItem("password", password);
}

bool BoxApi::register_new_user(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (status == A_REGISTER_NEW_USER_OK) {
        m_auth_token = docElem.elementsByTagName("auth_token").at(0).toElement().text();

        m_settings.store_ticket(m_auth_token);
        m_settings.store_auth_token(m_auth_token);

        return true;
    }

    getErrorString(status);
    return false;
}

void BoxApi::verify_registration_email_params(QUrl *url, QString email_address) {
    url->addQueryItem("action", A_VERIFY_REG_EMAIL);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("login", email_address);
}

bool BoxApi::verify_registration_email(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (status ==  A_VERIFY_REG_EMAIL_OK)
        return true;

    getErrorString(status);
    return false;
}

void BoxApi::get_account_info_params(QUrl *url) {
    url->addQueryItem("action", A_GET_ACC_INFO);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
}

QList<QVariant> BoxApi::get_account_info(QNetworkReply *networkReply) {
    QList<QVariant> userInfo;

    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, A_GET_ACC_INFO_OK, Qt::CaseSensitive)) {
        QDomNodeList __usernode = docElem.elementsByTagName("user").at(0).childNodes();
        /*!
            <user>
            <login>example@gmail.com</login>
            <email>example@gmail.com</email>
            <access_id>185381382</access_id>
            <user_id>185381382</user_id>
            <space_amount>5368709120</space_amount>
            <space_used>0</space_used>
            <max_upload_size>104857600</max_upload_size>
            </user>
        */
        for(uint i=0;i<__usernode.length();i++)
            if (i <= 1)
                userInfo << __usernode.at(i).toElement().text();
            else
                userInfo << __usernode.at(i).toElement().text().toLongLong();
    } else
        getErrorString(status);

    return userInfo;
}

void BoxApi::get_account_tree_params(QUrl *url) {
    url->addQueryItem("action", FF_GET_ACC_TREE);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("folder_id", QString("%1").arg(m_current_folder_id));

    url->addQueryItem("params[]", "onelevel");
    url->addQueryItem("params[]", "nozip");
}

bool BoxApi::get_account_tree(QNetworkReply *networkReply) {    
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, FF_LISTING_OK, Qt::CaseSensitive)) {

        QDomNodeList __foldernode = docElem.elementsByTagName("tree").at(0).firstChild().childNodes();
        QVector<BoxListModel::BoxItem *> __items;

        QString tagName;
        uint j;

        for(uint i=0;i<__foldernode.length();i++) {
            QDomNodeList __nodeList = __foldernode.at(i).childNodes();
            for(j=0;j<__nodeList.length();j++) {
                tagName = __foldernode.at(i).toElement().tagName();
                /* do not support tags at the moment */
                if (tagName == "folders" || tagName == "files") {
                     __items.append(
                                 m_boxCache->createItemCache(__nodeList.at(j).toElement(),
                                                              tagName == "folders" ? BoxCache::FOLDER : tagName == "files" ? BoxCache::FILE : BoxCache::TAG
                                                                                     )
                                 );
                }
            }
        }

        m_boxCache->maybe_cache(m_current_folder_id, __items);

        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::get_versions_params(QUrl *url, qlonglong file_id) {
    url->addQueryItem("action", VH_GET_VERSIONS);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", "file");
    url->addQueryItem("target_id", QString::number(file_id));
}

bool BoxApi::get_versions(QNetworkReply *networkReply) {
    Q_UNUSED(networkReply)
    return false;
}

void BoxApi::create_folder_params(QUrl *url) {
    url->addQueryItem("action", FF_CREATE_FOLDER);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("parent_id", QString::number(m_current_folder_id));
    url->addQueryItem("name", m_folder_or_filename);
    url->addQueryItem("share", "0");
}

bool BoxApi::create_folder(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, FF_CREATE_FOLDER_OK, Qt::CaseSensitive)) {
        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::get_updates_params(QUrl *url, quint32 begin_timestamp, quint32 end_timestamp) {
    url->addQueryItem("action", M_GET_UPDATES);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("begin_timestamp", QString::number(begin_timestamp));
    url->addQueryItem("end_timestamp", QString::number(end_timestamp));
    url->addQueryItem("params[]", "nozip");
}

bool BoxApi::get_updates(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, M_GET_UPDATES_OK, Qt::CaseSensitive)) {
        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::rename_params(QUrl *url, QString target) {
    url->addQueryItem("action", FF_RENAME);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", target);
    url->addQueryItem("target_id", QString::number(target=="folder"?m_current_folder_id:m_object_id));
    url->addQueryItem("new_name", m_folder_or_filename);
}

bool BoxApi::rename(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, FF_RENAME_OK, Qt::CaseSensitive)) {
        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::delete_params(QUrl *url, QString target, qlonglong item_id) {
    url->addQueryItem("action", FF_DELETE);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", target);
    if (item_id == -1)
        url->addQueryItem("target_id",  QString::number(target=="folder"?m_current_folder_id:m_object_id));
    else
        url->addQueryItem("target_id",  QString::number(item_id));
}

bool BoxApi::__delete(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, FF_DELETE_OK, Qt::CaseSensitive)) {
        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::copy_params(QUrl *url, qlonglong item_id) {
    url->addQueryItem("action", FF_COPY);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", "file");
    url->addQueryItem("target_id", QString::number(item_id));
    url->addQueryItem("destination_id", QString::number(m_current_folder_id));
}

bool BoxApi::copy(QNetworkReply *) {
    return false;
}

void BoxApi::move_params(QUrl *url, QString target, qlonglong item_id) {
    url->addQueryItem("action", FF_MOVE);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", target);
    url->addQueryItem("target_id", QString::number(item_id));
    url->addQueryItem("destination_id", QString::number(m_current_folder_id));
}

bool BoxApi::move(QNetworkReply*) {
    return false;
}

void BoxApi::publish_share_params(QUrl *url, int targetType) {
    url->addQueryItem("action", S_PUBLISH_SHARE);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", targetType?"folder":"file");
    url->addQueryItem("target_id", QString::number(targetType?m_current_folder_id:m_object_id));

    /*! those params should implement in the next version */
    url->addQueryItem("password", QString());
    url->addQueryItem("message", QString());
    url->addQueryItem("emails[]", QString());
}

bool BoxApi::publish_share(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, S_PUBLISH_SHARE_OK, Qt::CaseSensitive)) {
        QString publish_name;
        publish_name = docElem.elementsByTagName("public_name").at(0).toElement().text();
        m_share_link = QString("%1/%2").arg(S_PUBLISH_PREFIX_URL).arg(publish_name);
        return true;
    } else
        getErrorString(status);
    return false;
}


void BoxApi::publish_unshare_params(QUrl *url, int targetType) {
    url->addQueryItem("action", S_PUBLISH_UNSHARE);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", targetType?"folder":"file");
    url->addQueryItem("target_id", QString::number(targetType?m_current_folder_id:m_object_id));
}

bool BoxApi::publish_unshare(QNetworkReply *networkReply) {
    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, S_PUBLISH_UNSHARE_OK, Qt::CaseSensitive)) {
        return true;
    } else
        getErrorString(status);
    return false;
}

void BoxApi::get_comments_params(QUrl *url) {
    url->addQueryItem("action", CM_GET_COMMENTS);
    url->addQueryItem("api_key", m_api_key);
    url->addQueryItem("auth_token", m_auth_token);
    url->addQueryItem("target", "file");
    url->addQueryItem("target_id", QString::number(m_object_id));
}

QList<QVariant> BoxApi::get_comments(QNetworkReply *networkReply) {
    QList<QVariant> comments;

    m_domdocument->setContent(networkReply);
    QDomElement docElem = m_domdocument->documentElement();

    QString status = docElem.elementsByTagName("status").at(0).toElement().text();

    if (!QString::compare(status, CM_GET_COMMENTS_OK, Qt::CaseSensitive)) {
        QDomNodeList __commentnode = docElem.elementsByTagName("comments").at(0).childNodes();
        QStringList __tagNames;

        __tagNames << "user_name" << "message" << "created";

        for(uint i=0;i<__commentnode.length();i++) {
            QVariantList __comment;

            foreach(QString tagName, __tagNames) {
                QDomNodeList __qdNList =__commentnode.at(i).toElement().elementsByTagName(tagName);
                __comment << __qdNList.at(0).toElement().text();
            }

            comments << __comment;
        }
    } else
        getErrorString(status);
    return comments;
}

void BoxApi::getErrorString(QString status) {
    if (status == "application_restricted")
        errorString = QT_TR_NOOP("You provided an invalid api_key, or the api_key is restricted from calling this function");
    else if (status == "not_logged_in")
        errorString = QT_TR_NOOP("The user did not successfully authenticate on the page provided in the authentication process");
    else if (status == "e_folder_id")
        errorString = QT_TR_NOOP("Another error occured in your call");
    else if (status == "wrong_input")
        errorString = QT_TR_NOOP("When no api_key parameter is provided");
    else if (status == "get_auth_token_error")
        errorString = QT_TR_NOOP("Generic error for other invalid inputs");
    else if (status == "no_parent")
        errorString = QT_TR_NOOP("The parent folder does not exists");
    else if (status == "s_folder_exists")
        errorString = QT_TR_NOOP("A folder with the same name already exists in that location");
    else if (status == "invalid_folder_name")
        errorString = QT_TR_NOOP("The name provided for the new folder contained invalid characters or too many characters");
    else if (status == "e_no_folder_name")
        errorString = QT_TR_NOOP("A folder name was not properly provided");
    else if (status == "folder_name_too_big")
        errorString = QT_TR_NOOP("The folder name contained more than 100 characters, exceeding the folder name length limit");
    else if (status == "e_filename_in_use")
        errorString = QT_TR_NOOP("A file/folder of the same name already exists in the same folder");
    else if (status == "e_invalid_file_name")
        errorString = QT_TR_NOOP("The file/folder name contains invalid characters");
    else if (status == "e_file_name_too_big")
        errorString = QT_TR_NOOP("The file/folder name exceeds the number of characters");
    else if (status == "e_no_access")
        errorString = QT_TR_NOOP("The user does not have the necessary permissions to perform this action");
    else if (status == "e_no_target")
        errorString = QT_TR_NOOP("The target id either does not exist or is invalid");

    else if (status == "email_invalid")
        errorString = QT_TR_NOOP("Email is not a valid email address");
    else if (status == "email_already_registered")
        errorString = QT_TR_NOOP("Email is already registered by another user");

    else
        errorString = QT_TR_NOOP("An error has occurred");

    errorCode = status;
}

include(../globals.pri)

qmlsouce.source = qml/harbox
qmlsouce.target = qml
DEPLOYMENTFOLDERS = qmlsouce

CONFIG += qdeclarative-boostable
CONFIG += mobility

MOBILITY += feedback

QT += network xml dbus

SOURCES += src/main.cpp \
    src/controller.cpp \
    src/boxapi.cpp \
    src/boxlistmodel.cpp \
    src/settings.cpp \
    src/boxcache.cpp \
    src/filemodel.cpp \
    src/dinterface.cpp \
    src/boxupdatesmodel.cpp

include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

HEADERS += \
    src/controller.h \
    src/global.h \
    src/boxapi.h \
    src/boxlistmodel.h \
    src/settings.h \
    src/boxcache.h \
    src/filemodel.h \
    src/dinterface.h \
    src/boxupdatesmodel.h

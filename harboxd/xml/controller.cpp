/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c Controller -a controller.h:controller.cpp org.harboxd.filetransfer.xml
 *
 * qdbusxml2cpp is Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#include "controller.h"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/*
 * Implementation of adaptor class Controller
 */

Controller::Controller(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

Controller::~Controller()
{
    // destructor
}

void Controller::appendTest(const QVariantList &transferItems, int transferType)
{
    // handle method call org.harboxd.filetransfer.appendTest
    QMetaObject::invokeMethod(parent(), "appendTest", Q_ARG(QVariantList, transferItems), Q_ARG(int, transferType));
}

void Controller::showNativeFileTransferDialog()
{
    // handle method call org.harboxd.filetransfer.showNativeFileTransferDialog
    QMetaObject::invokeMethod(parent(), "showNativeFileTransferDialog");
}

void Controller::startTransfer()
{
    // handle method call org.harboxd.filetransfer.startTransfer
    QMetaObject::invokeMethod(parent(), "startTransfer");
}

void Controller::stop()
{
    // handle method call org.harboxd.filetransfer.stop
    QMetaObject::invokeMethod(parent(), "stop");
}


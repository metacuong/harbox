include(../globals.pri)

QT += network core dbus sql
QT -= gui

CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

PKGCONFIG += libtuiclient

TARGET = harboxd

TEMPLATE = app

contains(MEEGO_EDITION, harmattan) {
    target.path = /opt/harbox/bin
    services.files = ./meta/org.harboxd.filetransfer.service
    services.path = /usr/share/dbus-1/services
    INSTALLS += target services
}

SOURCES += \
    src/main.cpp \
    src/controller.cpp \
    ../harbox/src/settings.cpp \
    src/transferlogs.cpp

HEADERS += \
    src/controller.h \
    ../harbox/src/settings.h \
    src/transferlogs.h

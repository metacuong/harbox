/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "transferlogs.h"

#include <QDebug>

TransferLogs::TransferLogs():
    QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"))
{    
    m_dbFilePath = QString("%1/%2").arg(DEFAULT_TRANSFER_LOGS_PATH).arg(TRANSFER_LOGS_DBFILE);

    ensureDBCreated();
}

TransferLogs::~TransferLogs() {
    if (open())
        close();
}

void TransferLogs::ensureDBCreated() {
    QDir qDir;
    if (!qDir.exists(DEFAULT_TRANSFER_LOGS_PATH))
        qDir.mkdir(DEFAULT_TRANSFER_LOGS_PATH);

    setDatabaseName(m_dbFilePath);

    if (!QFile::exists(m_dbFilePath)) {
        if (open()) {
            QSqlQuery qSqlQuery;
            foreach(QString table, createTables())
                qSqlQuery.exec(table);
        }
    } else
        open();
}

QStringList TransferLogs::createTables() {
    QStringList qStringList;

    qStringList <<
                            "CREATE TABLE \"logs\" ("
                            "\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                            "\"name\" VARCHAR(255) NOT NULL,"
                            "\"size\" INTEGER NOT NULL,"
                            "\"type\" INTEGER NOT NULL,"
                            "\"result\" INTEGER NOT NULL,"
                            "\"datetime\" INTEGER NOT NULL,"
                            "\"note\" TEXT);";

    return qStringList;
}

/*!
  * @def writeLog
  * @param
  * filename : download/upload filename
  * filesize : size of that file
  * _status : completed | failed | cancelled
  * _type : upload | download
  * _note : write anything here
  * @return
  * void()
  */

void TransferLogs::writeLog(QString filename, quint32 filesize, Status _status, Type _type, QString _note) {
    if (isOpen()) {
        QSqlQuery qSqlQuery;
        QDateTime datetime;
        qSqlQuery.prepare("INSERT INTO logs (name, size, type, result, datetime, note) VALUES (:name, :size, :type, :result, :datetime, :note)");
        qSqlQuery.bindValue(":name",filename);
        qSqlQuery.bindValue(":size",filesize);
        qSqlQuery.bindValue(":type",_type);
        qSqlQuery.bindValue(":result",_status);
        qSqlQuery.bindValue(":datetime", datetime.currentDateTime().toTime_t());
        qSqlQuery.bindValue(":note",_note);
        qSqlQuery.exec();
    }
}

void TransferLogs::clearAllLogs() {
    if (isOpen()) {
        QSqlQuery qSqlQuery("DELETE FROM logs");
        qSqlQuery.exec();
    }
}

QVariantList TransferLogs::getAllLogs() const {
    QVariantList qVarList;
    if (isOpen()) {
        QSqlQuery qSqlQuery("SELECT name, size, type, result, datetime, note FROM logs ORDER BY datetime DESC");
        if (qSqlQuery.exec() && qSqlQuery.isSelect()) {
            while(qSqlQuery.next()) {
                QVariantList qStrList;
                qStrList << qSqlQuery.value(0).toString();
                qStrList << qSqlQuery.value(1).toLongLong();
                qStrList << qSqlQuery.value(2).toInt();
                qStrList << qSqlQuery.value(3).toInt();
                qStrList << qSqlQuery.value(4).toLongLong();
                qStrList << qSqlQuery.value(5).toString();
                qVarList << qStrList;
            }
        }
    }
    return qVarList;
}

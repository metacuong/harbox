/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QTimer>
#include <QStringList>
#include <QFile>
#include <QHash>

#include <TransferUI/Client>
#include <TransferUI/Transfer>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include "../harbox/src/settings.h"
#include "../harbox/src/global.h"
#include "transferlogs.h"

using namespace TransferUI;

namespace TransferUI {

class Client;
class Transfer;
class TransferUITransferItem;

}

class Controller : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.harboxd.filetransfer")
public:
    explicit Controller(QObject *parent = 0);
    ~Controller();
    
signals:
    void transfers_completed();
    void upload_completed(const QVariantList&);

public slots:   

    Q_NOREPLY void stop();
    Q_NOREPLY void showNativeFileTransferDialog();
    Q_NOREPLY void appendTransferItems(const QVariantList &, const int&);
    Q_NOREPLY void startTransfer();

    QVariantList getAllLogs() const;
    Q_NOREPLY void clearLogs();


    void finished(QNetworkReply*);
    void downloadProgress(qint64, qint64);
    void uploadProgress(qint64, qint64);
    void cancelTransfer(Transfer*);
    void networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility);

private:
    enum State{
        UPLOAD,
        DOWNLOAD
    };

    QNetworkAccessManager *m_networkaccessmanager;
    QNetworkReply *networkReply;
    TransferLogs transferLogs;

    Settings m_settings;
    
    Client *m_client;
    QList<QList<QVariant> >m_transferItems;

    QString m_api_key;
    QString m_auth_token;
    QString m_box_local_path;

    QFile m_file;
    quint32 m_folder_id;
    quint32 m_file_size;
    QVariantList m_folders_id;
    QString m_filename_for_download;
    QString m_filename_for_upload;

    QHash<QString, Transfer*> vTransfers;
    Transfer *m_ptr_transfer;
    QString m_current_transferID;

    QString registerTransfer(QString, quint32, int);

    bool m_is_busy;
    bool m_is_cancelled;
    State m_state;

    void continueTransfer();
    void saveFile(QNetworkReply*);
    void writeLog(TransferLogs::Status, QString _note = "");

};

#endif // CONTROLLER_H

/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "controller.h"
#include <QDebug>

Controller::Controller(QObject *parent) :
    QObject(parent),
    m_networkaccessmanager(new QNetworkAccessManager(this)),
    m_client(new Client(this))
{
    QDBusConnection::sessionBus().registerObject(R_OBJECT, this, QDBusConnection::ExportAllSlots | QDBusConnection::ExportAllSignals);
    QDBusConnection::sessionBus().registerService(R_SERVICE);

    QObject::connect(m_networkaccessmanager, SIGNAL(finished(QNetworkReply*)), SLOT(finished(QNetworkReply*)));
    QObject::connect(m_networkaccessmanager, SIGNAL(networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)),
                     SLOT(networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)));
    QObject::connect(m_client, SIGNAL(cancelTransfer(Transfer*)), SLOT(cancelTransfer(Transfer*)));    

    m_api_key = m_settings.get_api_key();
    m_auth_token = m_settings.get_auth_token();
    m_box_local_path = m_settings.get_box_local_path();

    m_is_busy = false;
    m_is_cancelled = false;

    m_client->init();       

}

Controller::~Controller() {
    delete m_client;
    delete m_networkaccessmanager;
    QDBusConnection::sessionBus().unregisterObject(R_OBJECT);
    QDBusConnection::sessionBus().unregisterService(R_SERVICE);
}

void Controller::stop() {
    if (m_is_busy)
        QTimer::singleShot(10000, this, SLOT(stop()));
    else
        exit(0);
}

void Controller::showNativeFileTransferDialog() {
    m_client->showUI();
}

/*!
  @param
  transferType = 0 -> DOWNLOAD
  transferType = 1 -> UPLOAD
  transferType = 2 -> OVERWRITE
  transferType = 3 -> NEW_COPY
  */
void Controller::appendTransferItems(const QVariantList &transferItems, const int &transferType) {
    QString transferID = registerTransfer(transferItems.at(0).toString(), transferItems.at(1).toLongLong(), transferType);

    QList<QVariant> item;
    item << transferItems << transferType << transferID;
    m_transferItems << item;

    if (!m_is_busy)
        startTransfer();
}

QString Controller::registerTransfer(QString fileName, quint32 fileSize, int transferType) {
    Transfer *transfer = m_client->registerTransfer("Box.net", transferType?TransferUI::Client::TRANSFER_TYPES_UPLOAD:TransferUI::Client::TRANSFER_TYPES_DOWNLOAD);

    transfer->waitForCommit();
    transfer->setIcon("icon-m-content-file-unknown");
    transfer->setTargetName(fileName);
    transfer->setSize(fileSize);
    transfer->setFilesCount(1);
    transfer->commit();

    vTransfers.insert(transfer->transferId(), transfer);

    return transfer->transferId();
}

void Controller::startTransfer() {
        continueTransfer();
}

void Controller::continueTransfer() {

    if (m_transferItems.count()) {
        /*!
            (QVariant(QString, "hosts.txt") ,  QVariant(uint, 1536) ,  QVariant(uint, 3023009133) ,  QVariant(int, 0) )
        */

         m_is_busy = true;
         m_is_cancelled = false;

         QList<QVariant> item = m_transferItems.front();

         m_current_transferID = item[4].toString();
         m_ptr_transfer = vTransfers.value(m_current_transferID);
         m_ptr_transfer->setActive(0.0);

         m_state = item[3].toInt()?UPLOAD:DOWNLOAD;

         QString strUrl = QString("%1/%2/%3/%4")
                 .arg(item[3].toInt()?UPLOAD_URL:DOWNLOAD_URL)
                 .arg(item[3].toInt()?"upload":"download")
                 .arg(m_auth_token)
                 .arg(item[2].toLongLong());
         QUrl url(strUrl);

          if (!item[3].toInt()) {
              m_filename_for_download = item[0].toString();
              networkReply = m_networkaccessmanager->get(QNetworkRequest(url));
          }
          else {
              m_file.setFileName(item[0].toString());
              m_folder_id = item[2].toLongLong();
              m_file_size = item[1].toLongLong();

              if (m_file.open(QFile::ReadOnly)) {

                  QStringList fileSplit = item[0].toString().split("/");
                  QString filename = fileSplit[fileSplit.count()-1];
                  m_filename_for_upload = item[0].toString();

                  QString boundary = "---------------------------916552054217479323188478885";

                  QByteArray parts(QString("--" + boundary + "\r\n").toAscii());
                  parts += "Content-Disposition: form-data; name=\"fichier\"; filename=\""+filename.toUtf8() +"\"\r\n";
                  parts += "Content-Type: application/octet-stream\r\n\r\n";
                  parts += m_file.readAll();
                  parts += "\r\n";
                  parts += QString("--" + boundary + "\r\n").toAscii();
                  parts += "Content-Disposition: form-data; name=\"upload\"\r\n\r\n";
                  parts += "Uploader\r\n";
                  parts += QString("--" + boundary + "--\r\n").toAscii();

                  m_file.close();

                  url.addQueryItem("share", "0");
                  url.addQueryItem("message", QString());
                  url.addQueryItem("emails[]", QString());

                  QNetworkRequest networkRequest(url);
                  networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "multipart/form-data; boundary=" + boundary);
                  networkRequest.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(parts.length()));

                  networkReply = m_networkaccessmanager->post(networkRequest, parts);
              } else {
                  m_transferItems.pop_front();
                  continueTransfer();
              }
          }

         if (!item[3].toInt())
             QObject::connect(networkReply,SIGNAL(downloadProgress(qint64, qint64)), SLOT(downloadProgress(qint64, qint64)));
         else
             QObject::connect(networkReply,SIGNAL(uploadProgress(qint64, qint64)), SLOT(uploadProgress(qint64, qint64)));

    } else {
        m_is_busy = false;
        m_is_cancelled = false;

        if (m_folders_id.count()) {
            QVariantList::Iterator iter = m_folders_id.begin();
            QVariant __t = *iter;
            ++iter;
            while(iter != m_folders_id.end()) {
                if (__t == *iter)
                    iter = m_folders_id.erase(iter);
                else {
                    __t = *iter;
                    ++iter;
                }
            }
            emit upload_completed(m_folders_id);
            m_folders_id.clear();
        }
        emit transfers_completed();
    }
}

void Controller::downloadProgress(qint64 received, qint64 total) {
    m_ptr_transfer->setProgress(((received*100) / total)*0.01);
}

void Controller::uploadProgress(qint64 sent, qint64 total) {
    m_ptr_transfer->setProgress(((sent*100) / total)*0.01);
}

void Controller::cancelTransfer(Transfer *transfer) {
    Q_UNUSED(transfer)

    QNetworkReply *__networkReply=dynamic_cast<QNetworkReply*>(networkReply);
    if (__networkReply->isRunning()) {
        m_is_cancelled = true;
        __networkReply->abort();
    }
}

void Controller::finished(QNetworkReply *networkReply) {
    if (m_is_cancelled) {
        m_ptr_transfer->markCancelled();
        writeLog(TransferLogs::CANCELLED);
    } else {
        if (!networkReply->error()) {
            switch(m_state) {
                case DOWNLOAD:
                    saveFile(networkReply);
                    break;
                case UPLOAD:
                    m_folders_id << m_folder_id;
                    break;
            }
            m_ptr_transfer->markCompleted(true);
            writeLog(TransferLogs::COMPLETED);
        } else {
            m_ptr_transfer->markFailure(QString(QT_TR_NOOP("FAILED")), networkReply->errorString());
            writeLog(TransferLogs::FAILED);
        }
    }

    m_transferItems.pop_front();
    continueTransfer();
}

void Controller::networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility networkAccessibility) {
    Q_UNUSED(networkAccessibility);
}

void Controller::saveFile(QNetworkReply *networkReply) {
    QFile qFile(QString("%1/%2").arg(m_box_local_path).arg(m_filename_for_download));
    if (qFile.open(QFile::WriteOnly)) {
        qFile.write(networkReply->readAll());        
        m_file_size = qFile.size();
        qFile.close();
    }
}

void Controller::writeLog(TransferLogs::Status _status, QString _note) {
    transferLogs.writeLog(
                m_state==UPLOAD?m_filename_for_upload:QString("%1/%2").arg(m_box_local_path).arg(m_filename_for_download),
                m_file_size,
                _status,
                m_state==DOWNLOAD?TransferLogs::DOWNLOAD:TransferLogs::UPLOAD,
                _note);
}

QVariantList Controller::getAllLogs() const {
    return transferLogs.getAllLogs();
}

void Controller::clearLogs() {
    transferLogs.clearAllLogs();
}

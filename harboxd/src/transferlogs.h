/*
 * HarBox - Unofficial Box.net client for MeeGo 1.2 Harmattan
 * Copyright (C) 2012 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TRANSFERLOGS_H
#define TRANSFERLOGS_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

#include <QDir>
#include <QVariant>
#include <QDateTime>

#include "../harbox/src/global.h"

#define TRANSFER_LOGS_DBFILE "transferlogs.db"

class TransferLogs : public QSqlDatabase
{
public:        
    TransferLogs();
    ~TransferLogs();

    enum Status {
        COMPLETED,
        FAILED,
        CANCELLED
    };

    enum Type {
        UPLOAD,
        DOWNLOAD
    };

    void writeLog(QString, quint32, Status, Type, QString);
    QVariantList getAllLogs() const;
    void clearAllLogs();

private:
    QString m_dbFilePath;

    void ensureDBCreated();
    QStringList createTables();
};

#endif // TRANSFERLOGS_H
